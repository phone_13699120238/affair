package com.zqf.generator;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.converts.MySqlTypeConvert;
import com.baomidou.mybatisplus.generator.config.po.TableFill;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.DbColumnType;
import com.baomidou.mybatisplus.generator.config.rules.IColumnType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import io.swagger.annotations.ApiModelProperty;
import org.junit.Test;
import org.junit.platform.commons.util.StringUtils;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * <p>
 * 测试生成代码
 * </p>
 */
public class GeneratorServiceEntity {

    // 修改数据库的链接
    private static final String DB_DRIVER_CLASS_NAME = "com.mysql.jdbc.Driver";
    private static final String DB_SCHEMA = "affair_v1";
    private static final String DB_URL = "jdbc:mysql://192.168.1.202:3306/" + DB_SCHEMA;
    private static final String DB_USERNAME = "mysql";
    private static final String DB_PASSWORD = "List@8384";

    // 生成类的包路径
    private static final String PACKAGE_NAME = "com.zy123.affair";
    // false:user -> UserService, 设置成true: user -> IUserService
    private static final boolean SERVICE_NAME_START_WITH_I = false;

    // 作者
    private static final String AUTHOR = "lishutao";

    // 源码生成的本地文件目录
    //private static final String SOURCE_CODE_OUT_PUT_DIR = "/Users/fwadmin/Downloads/hualala/workspace/codeGen";
    private static final String SOURCE_CODE_OUT_PUT_DIR = "D:\\codeGen";

    @Test
    public void generateCode() {
       String[] tables = new String[]{"affair_form","affair_form_attr","affair_form_data","affair_app","affair_app_menu","affair_app_view",
               "affair_view_page","affair_view_page_flow","affair_view_template","affair_flow","affair_flow_node","affair_flow_data","affair_flow_user"
        };
        String packageName =PACKAGE_NAME;
        generateByTables(SERVICE_NAME_START_WITH_I, packageName, tables);
    }


    private void generateByTables(boolean serviceNameStartWithI, String packageName, String... tableNames) {
        deleteFile(new File(SOURCE_CODE_OUT_PUT_DIR));
        deleteFile(new File(SOURCE_CODE_OUT_PUT_DIR));
        GlobalConfig config = new GlobalConfig();
        config.setDateType(DateType.ONLY_DATE);
        config.setSwagger2(true);
        DataSourceConfig dataSourceConfig = new DataSourceConfig();
        dataSourceConfig.setDbType(DbType.MYSQL)
                .setUrl(DB_URL)
                .setUsername(DB_USERNAME)
                .setPassword(DB_PASSWORD)
                .setTypeConvert(new MySqlTypeConvertCustom())
                .setDriverName("com.mysql.jdbc.Driver");
        StrategyConfig strategyConfig = new StrategyConfig();
        strategyConfig
                .setLogicDeleteFieldName("deleted")
                .setCapitalMode(true)
                .setEntityLombokModel(true)
                .setRestControllerStyle(true)
                .setColumnNaming(NamingStrategy.underline_to_camel)
                .setTablePrefix("affair_")
                .setNaming(NamingStrategy.underline_to_camel)
                .setEntityColumnConstant(true)
                .setInclude(tableNames);//修改替换成你需要的表名，多个表名传数组
        List<TableFill> tableFillList = getTableFills();
        strategyConfig.setTableFillList(tableFillList);
        config.setActiveRecord(true)
                .setAuthor(AUTHOR)
                .setOutputDir(SOURCE_CODE_OUT_PUT_DIR)
                .setFileOverride(true)
                .setEnableCache(false);
        if (!serviceNameStartWithI) {
            config.setServiceName("%sService");
        }
        new AutoGenerator().setGlobalConfig(config)
                .setDataSource(dataSourceConfig)
                .setStrategy(strategyConfig)
                .setPackageInfo(
                        new PackageConfig()
                                .setParent(packageName)
                                .setController("biz")
                                .setEntity("entity")
                ).execute();
    }

    private List<TableFill> getTableFills() {
        List<TableFill> tableFillList = new ArrayList<>();
        TableFill fill1 = new TableFill("create_user_id", FieldFill.INSERT);
        tableFillList.add(fill1);
        TableFill fill2  = new TableFill("create_time", FieldFill.INSERT);
        tableFillList.add(fill2);
        TableFill fill3 = new TableFill("deleted", FieldFill.INSERT);
        tableFillList.add(fill3);

        TableFill fill4 = new TableFill("update_user_id", FieldFill.INSERT_UPDATE);
        tableFillList.add(fill4);
        TableFill fill5 = new TableFill("update_time", FieldFill.INSERT_UPDATE);
        tableFillList.add(fill5);

        return tableFillList;
    }

    private static Set<String> concurrentTables() throws Exception{

        Set<String> tableNames = new TreeSet<>();

        String sql = "select table_name from information_schema.tables where table_schema = '" + DB_SCHEMA + "'";
        Class.forName(DB_DRIVER_CLASS_NAME);
        Connection connection = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);

        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);
        while (resultSet.next()) {
            String tableName = resultSet.getString(1);
            tableNames.add(tableName);
        }
        return tableNames;
    }


    private static void deleteFile(File file) {
        if (!file.exists()) {
            return;
        }
        file.setWritable(true);
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            if (files ==null || files.length <= 0) {
                file.delete();
                return;
            }
            for (File f : files) {
                deleteFile(f);
            }
        }
        file.delete();
    }

}


/**
 * 自定义类型转换
 */
class MySqlTypeConvertCustom extends MySqlTypeConvert implements ITypeConvert {
    @Override
    public IColumnType processTypeConvert(GlobalConfig globalConfig, String fieldType) {
        String t = fieldType.toLowerCase();
        if (t.contains("tinyint(1)")) {
            return DbColumnType.INTEGER;
        }
        return super.processTypeConvert(globalConfig, fieldType);
    }
}