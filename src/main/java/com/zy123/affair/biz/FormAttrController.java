package com.zy123.affair.biz;


import com.zy123.affair.common.PinyinUtil;
import com.zy123.affair.common.baseEntity.ResultObj;
import com.zy123.affair.common.baseEntity.SwaggerPage;
import com.zy123.affair.dto.*;
import com.zy123.affair.dto.FormAttrDto;
import com.zy123.affair.dto.FormAttrSearchDto;
import com.zy123.affair.service.FormAttrService;
import com.zy123.affair.service.FormAttrService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lishutao
 * @since 2022-09-22
 */
@Api(tags = "表单属性接口" )
@RestController
@RequestMapping("/formAttr")
public class FormAttrController {
    @Autowired
    private FormAttrService formAttrService;

    @ApiOperation("添加")
    @PostMapping("/add")
    public ResultObj add(@RequestBody FormAttrDto dto){
        //获取用户Id  appMenuId
        Long companyId=1L;
        formAttrService.add(companyId,dto);
        return new ResultObj<>();
    }

    @ApiOperation("修改")
    @PostMapping("/edit")
    public ResultObj edit(@RequestBody FormAttrDto dto){
        //获取用户Id  appMenuId
        Long companyId=1L;
        formAttrService.edit(companyId,dto);
        return new ResultObj<>();
    }

    @ApiOperation("删除")
    @PostMapping("/remove")
    public ResultObj remove(@RequestBody List<Long> idList){
        //获取用户Id  appMenuId
        Long companyId=1L;
        formAttrService.remove(companyId,idList);
        return new ResultObj<>();
    }


    @ApiOperation("详细")
    @GetMapping("/get/{id}")
    public ResultObj<FormAttrDto> get(@PathVariable("id") @ApiParam("主键Id") Long id){
        //获取用户Id  appMenuId
        Long companyId=1L;
        FormAttrDto appDto = formAttrService.get(companyId,id);
        return new ResultObj<>(appDto);
    }


    @ApiOperation("查询分页")
    @PostMapping("/search")
    public ResultObj<SwaggerPage<FormAttrDto>> search(@RequestBody FormAttrSearchDto searchDto){
        Long companyId=1L;
        SwaggerPage<FormAttrDto> page = formAttrService.search(companyId,searchDto);
        return new ResultObj<>(page);
    }

    @ApiOperation("查询列表")
    @PostMapping("/list")
    public ResultObj<List<FormAttrDto>> list(@RequestBody FormAttrSearchDto searchDto){
        //获取用户Id  appMenuId
        Long companyId=1L;

        List<FormAttrDto> list = formAttrService.list(companyId,searchDto);
        return new ResultObj<>(list);
    }


    @ApiOperation("详细列表")
    @PostMapping("/listByIdList")
    public ResultObj<List<FormAttrDto>> listByIdList(@RequestBody List<Long> idList){
        //获取用户Id  appMenuId
        Long companyId=1L;
        List<FormAttrDto> dtoList = formAttrService.listByIdList(companyId,idList);
        return new ResultObj<>(dtoList);
    }
}

