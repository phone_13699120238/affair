package com.zy123.affair.biz;


import com.zy123.affair.common.baseEntity.ResultObj;
import com.zy123.affair.common.baseEntity.SwaggerPage;
import com.zy123.affair.dto.ViewPageDto;
import com.zy123.affair.dto.ViewPageSearchDto;
import com.zy123.affair.service.ViewPageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lishutao
 * @since 2022-09-22
 */
@Api(tags = "页面设计接口" )
@RestController
@RequestMapping("/viewPage")
public class ViewPageController {
    @Autowired
    private ViewPageService viewPageService;

    @ApiOperation("保存设计")
    @PostMapping("/saveDesign")
    public ResultObj saveDesign(@RequestBody ViewPageDto dto){
        viewPageService.edit(dto);
        return new ResultObj<>();
    }

    @ApiOperation("详细")
    @GetMapping("/get/{id}")
    public ResultObj<ViewPageDto> get(@PathVariable("id") @ApiParam("主键Id") Long id){
        ViewPageDto appDto = viewPageService.get(id);
        return new ResultObj<>(appDto);
    }


    @ApiOperation("页面列表")
    @GetMapping("/listByAppMenuId/{appMenuId}")
    public ResultObj<List<ViewPageDto>> listByAppMenuId(@PathVariable("appMenuId") @ApiParam("菜单Id") Long appMenuId){
        List<ViewPageDto> list = viewPageService.listByAppMenuId(appMenuId);
        return new ResultObj<>(list);
    }


}

