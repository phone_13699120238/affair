package com.zy123.affair.biz;


import com.zy123.affair.common.baseEntity.ResultObj;
import com.zy123.affair.common.baseEntity.SwaggerPage;
import com.zy123.affair.dto.AppDto;
import com.zy123.affair.dto.AppSearchDto;
import com.zy123.affair.service.AppService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lishutao
 * @since 2022-09-22
 */
@Api(tags = "应用接口" )
@RestController
@RequestMapping("/app")
public class AppController {
    @Autowired
    private AppService appService;

    @ApiOperation("查询分页")
    @PostMapping("/search")
    public ResultObj<SwaggerPage<AppDto>> search(@RequestBody AppSearchDto searchDto){
        Long companyId=1L;
        SwaggerPage<AppDto> page = appService.search(searchDto);
        return new ResultObj<>(page);
    }

    @ApiOperation("添加")
    @PostMapping("/add")
    public ResultObj add(@RequestBody AppDto dto){
        Long companyId=1L;
        appService.add(dto);
        return new ResultObj<>();
    }

    @ApiOperation("修改")
    @PostMapping("/edit")
    public ResultObj edit(@RequestBody AppDto dto){
        Long companyId=1L;
        appService.edit(dto);
        return new ResultObj<>();
    }

    @ApiOperation("删除")
    @PostMapping("/remove")
    public ResultObj remove(@RequestBody List<Long> idList){
        Long companyId=1L;
        appService.remove(idList);
        return new ResultObj<>();
    }


    @ApiOperation("详细")
    @GetMapping("/get/{id}")
    public ResultObj<AppDto> get(@PathVariable("id") @ApiParam("主键Id") Long id){
        Long companyId=1L;
        AppDto appDto = appService.get(id);
        return new ResultObj<>(appDto);
    }


    @ApiOperation("查询列表")
    @PostMapping("/list")
    public ResultObj<List<AppDto>> list(@RequestBody AppSearchDto searchDto){
        List<AppDto> list = appService.list(searchDto);
        return new ResultObj<>(list);
    }


    @ApiOperation("详细列表")
    @PostMapping("/listByIdList")
    public ResultObj<List<AppDto>> listByIdList(@RequestBody List<Long> idList){
        List<AppDto> dtoList = appService.listByIdList(idList);
        return new ResultObj<>(dtoList);
    }
}

