package com.zy123.affair.biz;


import com.zy123.affair.common.baseEntity.ResultObj;
import com.zy123.affair.common.baseEntity.SwaggerPage;
import com.zy123.affair.dto.FormDataSearchDto;
import com.zy123.affair.service.FormAttrService;
import com.zy123.affair.service.FormDataService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lishutao
 * @since 2022-09-22
 */
@Api(tags = "表单数据接口" )
@RestController
@RequestMapping("/formData")
public class FormDataController {
    @Autowired
    private FormDataService formDataService;


    @ApiOperation("高级查询分页")
    @PostMapping("/advSearch/{formId}")
    public ResultObj<SwaggerPage<Map<String,Object>>> advSearch(@PathVariable("formId") @ApiParam("表单") Long formId,
                                                                @RequestBody Map<String,Object> searchDto){
        return new ResultObj<>();
    }

    @ApiOperation("查询分页")
    @PostMapping("/search/{formId}")
    public ResultObj<SwaggerPage<Map<String,Object>>> search(@PathVariable("formId") @ApiParam("表单") Long formId,
                                                              @RequestBody FormDataSearchDto searchDto){

        return new ResultObj<>();
    }

    @ApiOperation("添加")
    @PostMapping("/add/{formId}")
    public ResultObj add(@PathVariable("formId") @ApiParam("表单") Long formId,
                         @RequestBody Map<String,Object> map){
        return new ResultObj<>();
    }

    @ApiOperation("修改")
    @PostMapping("/edit/{formId}")
    public ResultObj edit(@PathVariable("formId") @ApiParam("表单") Long formId,
                          @RequestBody Map<String,Object> map){
        return new ResultObj<>();
    }

    @ApiOperation("删除")
    @PostMapping("/remove/{formId}")
    public ResultObj remove(@PathVariable("formId") @ApiParam("表单") Long formId,
                            @RequestBody List<Long> idList){
        return new ResultObj<>();
    }


    @ApiOperation("详细")
    @GetMapping("/get/{formId}/{id}")
    public ResultObj<Map<String,Object>> get(@PathVariable("formId") @ApiParam("表单") Long formId,
                                      @PathVariable("id") @ApiParam("主键Id") Long id){

        return new ResultObj<>();
    }


    @ApiOperation("查询列表")
    @PostMapping("/list/{formId}")
    public ResultObj<List<Map<String,Object>>> list(@PathVariable("formId") @ApiParam("表单") Long formId,
                                             @RequestBody FormDataSearchDto searchDto){

        return new ResultObj<>();
    }


    @ApiOperation("详细列表")
    @PostMapping("/listByIdList/{formId}")
    public ResultObj<List<Map<String,Object>>> listByIdList(@PathVariable("formId") @ApiParam("表单") Long formId,
                                                     @RequestBody List<Long> idList){
       // List<FormDataDto> dtoList = formDataService.listByIdList(idList);
        return new ResultObj<>();
    }
}

