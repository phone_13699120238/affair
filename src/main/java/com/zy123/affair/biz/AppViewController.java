package com.zy123.affair.biz;


import com.zy123.affair.common.baseEntity.ResultObj;
import com.zy123.affair.common.baseEntity.SwaggerPage;
import com.zy123.affair.dto.AppDto;
import com.zy123.affair.dto.AppSearchDto;
import com.zy123.affair.dto.AppViewDto;
import com.zy123.affair.dto.AppViewSearchDto;
import com.zy123.affair.service.AppService;
import com.zy123.affair.service.AppViewService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lishutao
 * @since 2022-09-22
 */
@Api(tags = "应用页面设置接口" )
@RestController
@RequestMapping("/appView")
public class AppViewController {
    @Autowired
    private AppViewService appViewService;

    @ApiOperation("保存页面配置")
    @PostMapping("/save")
    public ResultObj save(@RequestBody AppViewDto dto) {
        appViewService.save(dto);
        return new ResultObj<>();
    }

    @ApiOperation("保存页面模型")
    @PostMapping("/saveModel")
    public ResultObj saveModel(@RequestBody AppViewDto dto) {
        appViewService.saveModel(dto);
        return new ResultObj<>();
    }

    @ApiOperation("页面设置信息")
    @GetMapping("/get/{id}")
    public ResultObj<AppViewDto> get(@PathVariable("id") @ApiParam("主键Id") Long id) {
        AppViewDto appDto = appViewService.get(id);
        return new ResultObj<>(appDto);
    }

    @ApiOperation("页面模型信息")
    @GetMapping("/getModel/{id}")
    public ResultObj<AppViewDto> getModel(@PathVariable("id") @ApiParam("主键Id") Long id) {
        AppViewDto appDto = appViewService.getModel(id);
        return new ResultObj<>(appDto);
    }


}








