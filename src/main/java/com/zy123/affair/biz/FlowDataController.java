package com.zy123.affair.biz;


import com.zy123.affair.common.baseEntity.ResultObj;
import com.zy123.affair.common.baseEntity.SwaggerPage;
import com.zy123.affair.dto.FlowDataDto;
import com.zy123.affair.dto.FlowDataSearchDto;
import com.zy123.affair.service.FlowDataService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lishutao
 * @since 2022-09-22
 */
@Api(tags = "流程节点数据接口" )
@RestController
@RequestMapping("/flowData")
public class FlowDataController {
    @Autowired
    private FlowDataService flowDataService;

    @ApiOperation("添加")
    @PostMapping("/add")
    public ResultObj add(@RequestBody FlowDataDto dto){
        //获取用户Id  appMenuId
        Long companyId=1L;

        flowDataService.add(companyId,dto);
        return new ResultObj<>();
    }

    @ApiOperation("修改")
    @PostMapping("/edit")
    public ResultObj edit(@RequestBody FlowDataDto dto){
        //获取用户Id  appMenuId
        Long companyId=1L;
        flowDataService.edit(companyId,dto);
        return new ResultObj<>();
    }

    @ApiOperation("删除")
    @PostMapping("/remove")
    public ResultObj remove(@RequestBody List<Long> idList){
        //获取用户Id  appMenuId
        Long companyId=1L;
        flowDataService.remove(companyId,idList);
        return new ResultObj<>();
    }


    @ApiOperation("详细")
    @GetMapping("/get/{id}")
    public ResultObj<FlowDataDto> get(@PathVariable("id") @ApiParam("主键Id") Long id){
        //获取用户Id  appMenuId
        Long companyId=1L;
        FlowDataDto appDto = flowDataService.get(companyId,id);
        return new ResultObj<>(appDto);
    }


    @ApiOperation("查询分页")
    @PostMapping("/search")
    public ResultObj<SwaggerPage<FlowDataDto>> search(@RequestBody FlowDataSearchDto searchDto){
        Long companyId=1L;
        SwaggerPage<FlowDataDto> page = flowDataService.search(companyId,searchDto);
        return new ResultObj<>(page);
    }

    @ApiOperation("查询列表")
    @PostMapping("/list")
    public ResultObj<List<FlowDataDto>> list(@RequestBody FlowDataSearchDto searchDto){
        //获取用户Id  appMenuId
        Long companyId=1L;

        List<FlowDataDto> list = flowDataService.list(companyId,searchDto);
        return new ResultObj<>(list);
    }


    @ApiOperation("详细列表")
    @PostMapping("/listByIdList")
    public ResultObj<List<FlowDataDto>> listByIdList(@RequestBody List<Long> idList){
        //获取用户Id  appMenuId
        Long companyId=1L;
        List<FlowDataDto> dtoList = flowDataService.listByIdList(companyId,idList);
        return new ResultObj<>(dtoList);
    }
}

