package com.zy123.affair.biz;


import com.zy123.affair.common.baseEntity.ResultObj;
import com.zy123.affair.common.baseEntity.SwaggerPage;
import com.zy123.affair.dto.FlowUserDto;
import com.zy123.affair.dto.FlowUserSearchDto;
import com.zy123.affair.service.FlowUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lishutao
 * @since 2022-09-22
 */
@Api(tags = "流程中用户信息接口" )
@RestController
@RequestMapping("/flowUser")
public class FlowUserController {
    @Autowired
    private FlowUserService flowUserService;

    @ApiOperation("添加")
    @PostMapping("/add")
    public ResultObj add(@RequestBody FlowUserDto dto){
        //获取用户Id  appMenuId
        Long companyId=1L;

        flowUserService.add(companyId,dto);
        return new ResultObj<>();
    }

    @ApiOperation("修改")
    @PostMapping("/edit")
    public ResultObj edit(@RequestBody FlowUserDto dto){
        //获取用户Id  appMenuId
        Long companyId=1L;
        flowUserService.edit(companyId,dto);
        return new ResultObj<>();
    }

    @ApiOperation("删除")
    @PostMapping("/remove")
    public ResultObj remove(@RequestBody List<Long> idList){
        //获取用户Id  appMenuId
        Long companyId=1L;
        flowUserService.remove(companyId,idList);
        return new ResultObj<>();
    }


    @ApiOperation("详细")
    @GetMapping("/get/{id}")
    public ResultObj<FlowUserDto> get(@PathVariable("id") @ApiParam("主键Id") Long id){
        //获取用户Id  appMenuId
        Long companyId=1L;
        FlowUserDto appDto = flowUserService.get(companyId,id);
        return new ResultObj<>(appDto);
    }


    @ApiOperation("查询分页")
    @PostMapping("/search")
    public ResultObj<SwaggerPage<FlowUserDto>> search(@RequestBody FlowUserSearchDto searchDto){
        Long companyId=1L;
        SwaggerPage<FlowUserDto> page = flowUserService.search(companyId,searchDto);
        return new ResultObj<>(page);
    }

    @ApiOperation("查询列表")
    @PostMapping("/list")
    public ResultObj<List<FlowUserDto>> list(@RequestBody FlowUserSearchDto searchDto){
        //获取用户Id  appMenuId
        Long companyId=1L;

        List<FlowUserDto> list = flowUserService.list(companyId,searchDto);
        return new ResultObj<>(list);
    }


    @ApiOperation("详细列表")
    @PostMapping("/listByIdList")
    public ResultObj<List<FlowUserDto>> listByIdList(@RequestBody List<Long> idList){
        //获取用户Id  appMenuId
        Long companyId=1L;
        List<FlowUserDto> dtoList = flowUserService.listByIdList(companyId,idList);
        return new ResultObj<>(dtoList);
    }
}

