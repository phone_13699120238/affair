package com.zy123.affair.biz;


import com.zy123.affair.common.baseEntity.ResultObj;
import com.zy123.affair.common.baseEntity.SwaggerPage;
import com.zy123.affair.dto.FlowDto;
import com.zy123.affair.dto.FlowSearchDto;
import com.zy123.affair.service.FlowService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lishutao
 * @since 2022-09-22
 */
@Api(tags = "流程模型接口" )
@RestController
@RequestMapping("/flow")
public class FlowController {
    @Autowired
    private FlowService flowService;

    @ApiOperation("添加")
    @PostMapping("/add")
    public ResultObj add(@RequestBody FlowDto dto){
        //获取用户Id  appMenuId
        Long companyId=1L;

        flowService.add(companyId,dto);
        return new ResultObj<>();
    }

    @ApiOperation("修改")
    @PostMapping("/edit")
    public ResultObj edit(@RequestBody FlowDto dto){
        //获取用户Id  appMenuId
        Long companyId=1L;
        flowService.edit(companyId,dto);
        return new ResultObj<>();
    }

    @ApiOperation("删除")
    @PostMapping("/remove")
    public ResultObj remove(@RequestBody List<Long> idList){
        //获取用户Id  appMenuId
        Long companyId=1L;
        flowService.remove(companyId,idList);
        return new ResultObj<>();
    }


    @ApiOperation("详细")
    @GetMapping("/get/{id}")
    public ResultObj<FlowDto> get(@PathVariable("id") @ApiParam("主键Id") Long id){
        //获取用户Id  appMenuId
        Long companyId=1L;
        FlowDto appDto = flowService.get(companyId,id);
        return new ResultObj<>(appDto);
    }


    @ApiOperation("查询分页")
    @PostMapping("/search")
    public ResultObj<SwaggerPage<FlowDto>> search(@RequestBody FlowSearchDto searchDto){
        Long companyId=1L;
        SwaggerPage<FlowDto> page = flowService.search(companyId,searchDto);
        return new ResultObj<>(page);
    }

    @ApiOperation("查询列表")
    @PostMapping("/list")
    public ResultObj<List<FlowDto>> list(@RequestBody FlowSearchDto searchDto){
        //获取用户Id  appMenuId
        Long companyId=1L;

        List<FlowDto> list = flowService.list(companyId,searchDto);
        return new ResultObj<>(list);
    }


    @ApiOperation("详细列表")
    @PostMapping("/listByIdList")
    public ResultObj<List<FlowDto>> listByIdList(@RequestBody List<Long> idList){
        //获取用户Id  appMenuId
        Long companyId=1L;
        List<FlowDto> dtoList = flowService.listByIdList(companyId,idList);
        return new ResultObj<>(dtoList);
    }
}

