package com.zy123.affair.biz;


import com.zy123.affair.common.baseEntity.ResultObj;
import com.zy123.affair.common.baseEntity.SwaggerPage;
import com.zy123.affair.dto.AppMenuDto;
import com.zy123.affair.dto.FormSearchDto;
import com.zy123.affair.dto.TreeNodeDto;
import com.zy123.affair.service.FormService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lishutao
 * @since 2022-09-22
 */
@Api(tags = "表单接口" )
@RestController
@RequestMapping("/form")
public class FormController {
    @Autowired
    private FormService formService;

    @ApiOperation("获取完整树")
    @GetMapping("/tree")
    public ResultObj<List<TreeNodeDto>> tree(){
        //获取用户Id  appMenuId
        Long companyId=1L;
        List<TreeNodeDto> list = formService.tree(companyId);
        return new ResultObj<>(list);
    }

    @ApiOperation("添加节点")
    @PostMapping("/addNode")
    public ResultObj add(@RequestBody TreeNodeDto nodeDto){
        Long companyId=1L;
        formService.addNode(companyId,nodeDto);
        return new ResultObj<>();
    }

    @ApiOperation("修改节点")
    @PostMapping("/editNode")
    public ResultObj editNode(@RequestBody TreeNodeDto nodeDto){
        Long companyId=1L;
        formService.editNode(companyId,nodeDto);
        return new ResultObj<>();
    }

    @ApiOperation("删除节点")
    @PostMapping("/removeNode")
    public ResultObj removeNode(@RequestBody List<Long> idList){
        Long companyId=1L;
        formService.removeNode(companyId,idList);
        return new ResultObj<>();
    }



}

