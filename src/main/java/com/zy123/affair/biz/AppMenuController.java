package com.zy123.affair.biz;


import com.zy123.affair.common.baseEntity.ResultObj;
import com.zy123.affair.dto.AppMenuDto;
import com.zy123.affair.dto.TreeNodeDto;
import com.zy123.affair.service.AppMenuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lishutao
 * @since 2022-09-22
 */
@Api(tags = "应用导航接口" )
@RestController
@RequestMapping("/appMenu")
public class AppMenuController {
    @Autowired
    private AppMenuService appMenuService;

    @ApiOperation("获取完整树")
    @GetMapping("/tree/{appId}")
    public ResultObj<List<TreeNodeDto>> tree(@PathVariable("appId") @ApiParam("应用Id") Long appId){
        //获取用户Id  appMenuId
        Long companyId=1L;
        List<TreeNodeDto> list = appMenuService.tree(appId);
        return new ResultObj<>(list);
    }

    @ApiOperation("添加节点")
    @PostMapping("/addNode/{appId}")
    public ResultObj add(@PathVariable("appId") @ApiParam("应用Id") Long appId,@RequestBody TreeNodeDto nodeDto){
        Long companyId=1L;
        appMenuService.addNode(appId,nodeDto);
        return new ResultObj<>();
    }

    @ApiOperation("修改节点")
    @PostMapping("/editNode")
    public ResultObj editNode(@RequestBody TreeNodeDto nodeDto){
        Long companyId=1L;
        appMenuService.editNode(nodeDto);
        return new ResultObj<>();
    }

    @ApiOperation("删除节点")
    @PostMapping("/removeNode/{appId}")
    public ResultObj remove(@PathVariable("appId") @ApiParam("应用Id") Long appId,@RequestBody List<Long> idList){
        Long companyId=1L;
        appMenuService.removeNode(appId,idList);
        return new ResultObj<>();
    }


    @ApiOperation("修改")
    @PostMapping("/edit")
    public ResultObj edit(@RequestBody AppMenuDto dto){
        Long companyId=1L;
        appMenuService.edit(dto);
        return new ResultObj<>();
    }

    @ApiOperation("详细")
    @GetMapping("/get")
    public ResultObj<AppMenuDto> get(@PathVariable("id") @ApiParam("主键Id") Long id){
        Long companyId=1L;
        AppMenuDto appDto = appMenuService.get(id);
        return new ResultObj<>(appDto);
    }
}

