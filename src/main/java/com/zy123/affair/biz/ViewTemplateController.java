package com.zy123.affair.biz;


import com.zy123.affair.common.baseEntity.ResultObj;
import com.zy123.affair.common.baseEntity.SwaggerPage;
import com.zy123.affair.dto.ViewTemplateDto;
import com.zy123.affair.dto.ViewTemplateSearchDto;
import com.zy123.affair.service.ViewTemplateService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lishutao
 * @since 2022-09-22
 */
@Api(tags = "通用模板接口" )
@RestController
@RequestMapping("/viewTemplate")
public class ViewTemplateController {
    @Autowired
    private ViewTemplateService viewTemplateService;

    @ApiOperation("查询分页")
    @PostMapping("/search")
    public ResultObj<SwaggerPage<ViewTemplateDto>> search(@RequestBody ViewTemplateSearchDto searchDto){
        SwaggerPage<ViewTemplateDto> page = viewTemplateService.search(searchDto);
        return new ResultObj<>(page);
    }

    @ApiOperation("添加")
    @PostMapping("/add")
    public ResultObj add(@RequestBody ViewTemplateDto dto){
        viewTemplateService.add(dto);
        return new ResultObj<>();
    }

    @ApiOperation("修改")
    @PostMapping("/edit")
    public ResultObj edit(@RequestBody ViewTemplateDto dto){
        viewTemplateService.edit(dto);
        return new ResultObj<>();
    }

    @ApiOperation("删除")
    @PostMapping("/remove")
    public ResultObj remove(@RequestBody List<Long> idList){
        viewTemplateService.remove(idList);
        return new ResultObj<>();
    }


    @ApiOperation("详细")
    @GetMapping("/get/{id}")
    public ResultObj<ViewTemplateDto> get(@PathVariable("id") @ApiParam("主键Id") Long id){
        ViewTemplateDto appDto = viewTemplateService.get(id);
        return new ResultObj<>(appDto);
    }


    @ApiOperation("查询列表")
    @PostMapping("/list")
    public ResultObj<List<ViewTemplateDto>> list(@RequestBody ViewTemplateSearchDto searchDto){
        List<ViewTemplateDto> list = viewTemplateService.list(searchDto);
        return new ResultObj<>(list);
    }


    @ApiOperation("详细列表")
    @PostMapping("/listByIdList")
    public ResultObj<List<ViewTemplateDto>> listByIdList(@RequestBody List<Long> idList){
        List<ViewTemplateDto> dtoList = viewTemplateService.listByIdList(idList);
        return new ResultObj<>(dtoList);
    }
}

