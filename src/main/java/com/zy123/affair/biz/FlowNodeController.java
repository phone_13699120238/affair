package com.zy123.affair.biz;


import com.zy123.affair.common.baseEntity.ResultObj;
import com.zy123.affair.common.baseEntity.SwaggerPage;
import com.zy123.affair.dto.FlowNodeDto;
import com.zy123.affair.dto.FlowNodeSearchDto;
import com.zy123.affair.service.FlowNodeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lishutao
 * @since 2022-09-22
 */
@Api(tags = "流程模型节点接口" )
@RestController
@RequestMapping("/flowNode")
public class FlowNodeController {
    @Autowired
    private FlowNodeService flowNodeService;

    @ApiOperation("添加")
    @PostMapping("/add")
    public ResultObj add(@RequestBody FlowNodeDto dto){
        //获取用户Id  appMenuId
        Long companyId=1L;
        flowNodeService.add(companyId,dto);
        return new ResultObj<>();
    }

    @ApiOperation("修改")
    @PostMapping("/edit")
    public ResultObj edit(@RequestBody FlowNodeDto dto){
        //获取用户Id  appMenuId
        Long companyId=1L;
        flowNodeService.edit(companyId,dto);
        return new ResultObj<>();
    }

    @ApiOperation("删除")
    @PostMapping("/remove")
    public ResultObj remove(@RequestBody List<Long> idList){
        //获取用户Id  appMenuId
        Long companyId=1L;
        flowNodeService.remove(companyId,idList);
        return new ResultObj<>();
    }


    @ApiOperation("详细")
    @GetMapping("/get/{id}")
    public ResultObj<FlowNodeDto> get(@PathVariable("id") @ApiParam("主键Id") Long id){
        //获取用户Id  appMenuId
        Long companyId=1L;
        FlowNodeDto appDto = flowNodeService.get(companyId,id);
        return new ResultObj<>(appDto);
    }


    @ApiOperation("查询分页")
    @PostMapping("/search")
    public ResultObj<SwaggerPage<FlowNodeDto>> search(@RequestBody FlowNodeSearchDto searchDto){
        Long companyId=1L;
        SwaggerPage<FlowNodeDto> page = flowNodeService.search(companyId,searchDto);
        return new ResultObj<>(page);
    }

    @ApiOperation("查询列表")
    @PostMapping("/list")
    public ResultObj<List<FlowNodeDto>> list(@RequestBody FlowNodeSearchDto searchDto){
        //获取用户Id  appMenuId
        Long companyId=1L;

        List<FlowNodeDto> list = flowNodeService.list(companyId,searchDto);
        return new ResultObj<>(list);
    }


    @ApiOperation("详细列表")
    @PostMapping("/listByIdList")
    public ResultObj<List<FlowNodeDto>> listByIdList(@RequestBody List<Long> idList){
        //获取用户Id  appMenuId
        Long companyId=1L;
        List<FlowNodeDto> dtoList = flowNodeService.listByIdList(companyId,idList);
        return new ResultObj<>(dtoList);
    }
}

