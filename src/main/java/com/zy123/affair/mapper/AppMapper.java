package com.zy123.affair.mapper;

import com.zy123.affair.entity.App;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 应用 Mapper 接口
 * </p>
 *
 * @author lishutao
 * @since 2022-11-07
 */
public interface AppMapper extends BaseMapper<App> {

}
