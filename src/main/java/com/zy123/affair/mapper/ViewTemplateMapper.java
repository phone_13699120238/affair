package com.zy123.affair.mapper;

import com.zy123.affair.entity.ViewTemplate;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 页面模板 Mapper 接口
 * </p>
 *
 * @author lishutao
 * @since 2022-11-07
 */
public interface ViewTemplateMapper extends BaseMapper<ViewTemplate> {

}
