package com.zy123.affair.mapper;

import com.zy123.affair.entity.AppMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 应用导航 Mapper 接口
 * </p>
 *
 * @author lishutao
 * @since 2022-11-07
 */
public interface AppMenuMapper extends BaseMapper<AppMenu> {

}
