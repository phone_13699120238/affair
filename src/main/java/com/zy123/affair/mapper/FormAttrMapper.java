package com.zy123.affair.mapper;

import com.zy123.affair.entity.FormAttr;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 表单数据 Mapper 接口
 * </p>
 *
 * @author lishutao
 * @since 2022-11-07
 */
public interface FormAttrMapper extends BaseMapper<FormAttr> {

}
