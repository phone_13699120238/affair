package com.zy123.affair.mapper;

import com.zy123.affair.entity.FlowData;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 流程节点数据 Mapper 接口
 * </p>
 *
 * @author lishutao
 * @since 2022-11-07
 */
public interface FlowDataMapper extends BaseMapper<FlowData> {

}
