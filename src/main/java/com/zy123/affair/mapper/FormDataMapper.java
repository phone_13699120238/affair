package com.zy123.affair.mapper;

import com.zy123.affair.entity.FormData;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 表单属性 Mapper 接口
 * </p>
 *
 * @author lishutao
 * @since 2022-11-07
 */
public interface FormDataMapper extends BaseMapper<FormData> {

}
