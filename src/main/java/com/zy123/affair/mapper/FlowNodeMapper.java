package com.zy123.affair.mapper;

import com.zy123.affair.entity.FlowNode;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 流程模板节点 Mapper 接口
 * </p>
 *
 * @author lishutao
 * @since 2022-11-07
 */
public interface FlowNodeMapper extends BaseMapper<FlowNode> {

}
