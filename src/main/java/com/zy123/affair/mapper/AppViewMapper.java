package com.zy123.affair.mapper;

import com.zy123.affair.entity.AppView;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 应用表单视图 Mapper 接口
 * </p>
 *
 * @author lishutao
 * @since 2022-11-07
 */
public interface AppViewMapper extends BaseMapper<AppView> {

}
