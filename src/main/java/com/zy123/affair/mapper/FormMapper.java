package com.zy123.affair.mapper;

import com.zy123.affair.entity.Form;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 事务 Mapper 接口
 * </p>
 *
 * @author lishutao
 * @since 2022-11-07
 */
public interface FormMapper extends BaseMapper<Form> {

}
