package com.zy123.affair.mapper;

import com.zy123.affair.entity.FlowUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 流程节点人员处理 Mapper 接口
 * </p>
 *
 * @author lishutao
 * @since 2022-11-07
 */
public interface FlowUserMapper extends BaseMapper<FlowUser> {

}
