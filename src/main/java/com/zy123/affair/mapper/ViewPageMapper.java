package com.zy123.affair.mapper;

import com.zy123.affair.entity.ViewPage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 模板页面 Mapper 接口
 * </p>
 *
 * @author lishutao
 * @since 2022-11-07
 */
public interface ViewPageMapper extends BaseMapper<ViewPage> {

}
