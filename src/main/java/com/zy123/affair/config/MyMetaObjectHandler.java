package com.zy123.affair.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;

/**
 *  注入公共字段自动填充,任选注入方式即可
 */
@Slf4j
public class MyMetaObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        Object createStamp = this.getFieldValByName("createUserId", metaObject);
        if (createStamp == null) {
          /*  UserLoginDto userLoginDto = TokenUserLocal.get();
            if(userLoginDto != null) {
                this.setFieldValByName("createUserId", userLoginDto.getUserId(), metaObject);
            }*/
        }

        Object createTime = this.getFieldValByName("createTime", metaObject);
        if (createTime == null) {
            this.setFieldValByName("createTime", System.currentTimeMillis(),metaObject);
        }

        Object deleted = this.getFieldValByName("deleted", metaObject);
        if (deleted == null) {
            this.setFieldValByName("deleted", 2,metaObject);
        }


        Object updateUserId = this.getFieldValByName("updateUserId", metaObject);
        if (updateUserId == null) {
           /* UserLoginDto userLoginDto = TokenUserLocal.get();
            if(userLoginDto != null) {
                this.setFieldValByName("updateUserId", userLoginDto.getUserId(), metaObject);
            }*/
        }

        Object updateTime = this.getFieldValByName("updateTime", metaObject);
        if (updateTime == null) {
            this.setFieldValByName("updateTime", System.currentTimeMillis(),metaObject);
        }

    }

    @Override
    public void updateFill(MetaObject metaObject) {
        Object updateUserId = this.getFieldValByName("updateUserId", metaObject);
      /*  if (updateUserId == null) {
            UserLoginDto userLoginDto = TokenUserLocal.get();
            if(userLoginDto != null) {
                this.setFieldValByName("updateUserId", userLoginDto.getUserId(), metaObject);
            }
        }*/

        Object updateTime = this.getFieldValByName("updateTime", metaObject);
        if (updateTime == null) {
            this.setFieldValByName("updateTime", System.currentTimeMillis(),metaObject);
        }

    }
}
