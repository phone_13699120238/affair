package com.zy123.affair.config.aop;

import com.zy123.affair.common.baseEntity.ResultObj;
import com.zy123.affair.common.exception.ParameterException;
import com.zy123.affair.common.exception.ServiceException;
import com.zy123.affair.common.exception.ToUserException;
import com.zy123.affair.common.jackson.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


@Aspect
@Component
@Slf4j
public class RestAop {
    /**
     * 声明一个切入点，可以拦截具体到某个方法， 即在执行此方法之前、之后、最终、异常……时可以执行的其他业务方法（通知advice）；
     * 括号内的意思是：拦截某个方法，返回值是所有类型（第一个*）， com.aoptest.service包及其子包下的所有类（..*），
     * 类下所有的方法（第三个.*），返回值任意（内部嵌套括号(..)）
     */
    @Pointcut("execution(* com.zqf.robottrain.biz..*.*(..))")
    private void anyMethod() {

    }

    // @Before(value="anyMethod()&&args(req,res,..)")
    // 声明前置通知
    public void doBefore(JoinPoint jp) {

    }

    // 声明后置通知
    // @AfterReturning(pointcut = "anyMethod()")
    public void doAfterReturning() {
        System.out.println("后置通知");
    }

    // 声明例外通知
    // @AfterThrowing(pointcut = "anyMethod()", throwing = "e")
    public void doAfterThrowing(JoinPoint jp, Exception e) {

    }

    // 声明最终通知
    // @After(value="anyMethod()")
    public void doAfter(JoinPoint jp) {

        // String methodName = jp.getSignature().getName();
    }

    // 声明环绕通知
    @Around("anyMethod()")
    public Object doAround(ProceedingJoinPoint pjp) throws Throwable {
        Object o;
        RequestAttributes ra = RequestContextHolder.getRequestAttributes();
        ServletRequestAttributes sra = (ServletRequestAttributes) ra;
        HttpServletRequest req = sra.getRequest();
        HttpServletResponse res = sra.getResponse();
        try {
            log.info("进入controller>>>>>>>>>>>>>>");
            log.info("请求地址 : " + req.getRequestURL().toString());
            //   log.info("请求IP : "+ getIp(req));
            log.info("请求方式 : " + pjp.getSignature().getName());
            log.info("请求体（body）:" + JsonUtils.toJson(pjp.getArgs()));
            log.info("请求参数：" + JsonUtils.toJson(req.getParameterMap()));
            o = pjp.proceed();
            log.info("返回结果：" + JsonUtils.toJson(o));
            return o;
        }  catch (ParameterException e) {
            ResultObj<Object> result = new ResultObj<>(e.errorCode, e.getMessage());
            writeString(JsonUtils.toJson(result), res);
            return null;
        } catch (ServiceException e) {
            ResultObj<Object> result = new ResultObj<>(e.errorCode, e.getMessage());
            writeString(JsonUtils.toJson(result), res);
            return null;
        } catch (ToUserException e) {
            ResultObj<Object> result = new ResultObj<>(e.errorCode, e.getMessage());
            writeString(JsonUtils.toJson(result), res);
            return null;
        }catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }

    public  void writeString(String str, HttpServletResponse res) {
        res.setContentType("application/json;charset=UTF-8");
        PrintWriter out;
        try {
            out = res.getWriter();
            out.write(str);
            out.flush();
            out.close();
        } catch (IOException e) {

        }
    }
}
