package com.zy123.affair.config.log;

import feign.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LogConfiguration {
    @Value("${feign.level.val:full}")
    private Logger.Level feignLevel;

    @Bean
    Logger.Level feignLoggerLevel(){
        return feignLevel;
    }

}