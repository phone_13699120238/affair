package com.zy123.affair.dto;

import com.zy123.affair.entity.formAttr.FormAttrValueScope;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value="表单属性", description="表单属性")
@Data
public class FlowNodeDto {
    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "表单")
    private Long formId;

    @ApiModelProperty(value = "名字")
    private String name;

    @ApiModelProperty(value = "拼音名称")
    private String pyName;

    @ApiModelProperty(value = "类型 1:文本 2：整数 3：日期  4：小数  5：选择 6：表单 ")
    private Integer type;

    @ApiModelProperty(value = "关系： 1： 单值  2：多值")
    private Integer valueCountType;

    @ApiModelProperty(value = "范围值")
    private FormAttrValueScope formAttrSelect;

    @ApiModelProperty(value = "显示属性；type = 6 时,")
    private String showAttr;

}
