package com.zy123.affair.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author list
 */
@Data
@ApiModel(value="树节点对象", description="")
public class TreeNodeDto {
    private Long id;

    @ApiModelProperty(value = "父Id")
    private Long parentId;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "节点类型")
    private Integer nodeType;

    @ApiModelProperty(value = "子节点对象")
    private List<TreeNodeDto> list;
}
