package com.zy123.affair.dto;

import com.zy123.affair.entity.formAttr.FormAttrValueScope;
import com.zy123.affair.entity.x6.SceneViewModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value="表单属性", description="表单属性")
@Data
public class FlowDto {
    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "表单Id")
    private Long formId;

    @ApiModelProperty(value = "流程名称")
    private String name;

    @ApiModelProperty(value = "页面流程")
    private SceneViewModel viewModel;

}
