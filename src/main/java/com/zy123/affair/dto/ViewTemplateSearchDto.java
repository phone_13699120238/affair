package com.zy123.affair.dto;

import com.zy123.affair.common.baseEntity.SwaggerPage;
import io.swagger.annotations.ApiModel;
import lombok.Data;

@ApiModel(value="应用菜单", description="应用菜单")
@Data
public class ViewTemplateSearchDto extends SwaggerPage {
}
