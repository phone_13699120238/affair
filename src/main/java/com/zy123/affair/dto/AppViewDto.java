package com.zy123.affair.dto;

import com.zy123.affair.entity.x6.SceneViewModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value="应用页面设置", description="应用页面设置")
@Data
public class AppViewDto {
    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "表单Id")
    private Long formId;

    @ApiModelProperty(value = "应用菜单Id")
    private Long appMenuId;

    @ApiModelProperty(value = "页面视图模型")
    private String modelJson;
}
