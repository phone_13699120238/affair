package com.zy123.affair.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value="应用", description="应用")
@Data
public class AppDto {
    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "公司id")
    private Long companyId;

    @ApiModelProperty(value = "应用范围")
    private String userType;

    @ApiModelProperty(value = "名字")
    private String name;

    @ApiModelProperty(value = "备注说明")
    private String remark;

    @ApiModelProperty(value = "状态 1：正常 2：禁用")
    private Integer status;
}
