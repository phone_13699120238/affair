package com.zy123.affair.dto;

import com.zy123.affair.common.baseEntity.SwaggerPage;
import io.swagger.annotations.ApiModel;
import lombok.Data;

@ApiModel(value="表单属性查询", description="表单属性查询")
@Data
public class FlowDataSearchDto extends SwaggerPage {
    private Long formId;
}
