package com.zy123.affair.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@ApiModel(value="应用菜单", description="应用菜单")
@Data
public class AppMenuDto {
    Long companyId=1L;
}
