package com.zy123.affair.dto;

import com.zy123.affair.entity.viewPage.Design;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value="页面设计", description="页面设计")
@Data
public class ViewPageDto {
    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "应用视图Id")
    private Long appViewId;

    @ApiModelProperty(value = "模板Id")
    private Long viewTemplateId;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "页面设计")
    private Design design;

    @ApiModelProperty(value = "流程节点key")
    private String nodeKey;

    @ApiModelProperty(value = "页面类型： 1. 表格页面  2.表单页面 3. 表格树")
    private Integer type;
}
