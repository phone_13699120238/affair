package com.zy123.affair;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author admin
 */
@ServletComponentScan
@EnableDiscoveryClient
@EnableFeignClients
@SpringBootApplication
public class AffairApp {
    public static void main(String[] args) {
        SpringApplication.run(AffairApp.class, args);
    }
}

