package com.zy123.affair.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 流程节点数据
 * </p>
 *
 * @author lishutao
 * @since 2022-11-08
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("affair_flow_data")
@ApiModel(value="FlowData对象", description="流程节点数据")
public class FlowData extends Model<FlowData> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "流程节点Id")
    private Long flowNodeId;

    @ApiModelProperty(value = "表单数据Id")
    private Long formDataId;

    @ApiModelProperty(value = "1: 进行中 2：已处理 3：回退作废")
    private Integer dealStatus;

    @ApiModelProperty(value = "相关处置信息")
    private String dealRemark;

    @ApiModelProperty(value = "创建时间")
      @TableField(fill = FieldFill.INSERT)
    private Long createTime;

    @ApiModelProperty(value = "创建人")
      @TableField(fill = FieldFill.INSERT)
    private Long createUserId;

    @ApiModelProperty(value = "更新时间")
      @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateTime;

    @ApiModelProperty(value = "更新人")
      @TableField(fill = FieldFill.INSERT_UPDATE)
    private Integer updateUserId;

    @ApiModelProperty(value = "删除状态 1：删除 2：正常")
      @TableField(fill = FieldFill.INSERT)
    @TableLogic
    private Integer deleted;


    public static final String ID = "id";

    public static final String FLOW_NODE_ID = "flow_node_id";

    public static final String FORM_DATA_ID = "form_data_id";

    public static final String DEAL_STATUS = "deal_status";

    public static final String DEAL_REMARK = "deal_remark";

    public static final String CREATE_TIME = "create_time";

    public static final String CREATE_USER_ID = "create_user_id";

    public static final String UPDATE_TIME = "update_time";

    public static final String UPDATE_USER_ID = "update_user_id";

    public static final String DELETED = "deleted";

    @Override
    protected Serializable pkVal() {
        return null;
    }

}
