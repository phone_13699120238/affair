package com.zy123.affair.entity.x6;

import lombok.Data;

import java.util.List;

/**
 * @author list
 */
@Data
public class SceneViewModel<T> {
    private String id;
    private List<SceneViewCell<T>> cells;
}
