package com.zy123.affair.entity.x6;

import lombok.Data;

@Data
public class SceneViewLink {
    private String cell;
    private String port;
}
