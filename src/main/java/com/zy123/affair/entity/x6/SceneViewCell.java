package com.zy123.affair.entity.x6;

import lombok.Data;

@Data
public class SceneViewCell<T> {
    private String id;
    private T data;
    private String shape;
    private SceneViewLink target;
    private SceneViewLink source;
}
