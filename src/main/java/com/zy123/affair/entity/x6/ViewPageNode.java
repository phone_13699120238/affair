package com.zy123.affair.entity.x6;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author list
 */
@Data
public class ViewPageNode {
    @ApiModelProperty(value = "名称")
    private String name;
    @ApiModelProperty(value = "页面类型")
    private Integer type;
}
