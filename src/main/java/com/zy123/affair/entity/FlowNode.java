package com.zy123.affair.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 流程模板节点
 * </p>
 *
 * @author lishutao
 * @since 2022-11-08
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("affair_flow_node")
@ApiModel(value="FlowNode对象", description="流程模板节点")
public class FlowNode extends Model<FlowNode> {

    private static final long serialVersionUID = 1L;

      private Long id;

    @ApiModelProperty(value = "流程模板Id")
    private Long flowId;

    @ApiModelProperty(value = "页面视图Id")
    private String viewId;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "条件限制json")
    private String conditionJson;

    @ApiModelProperty(value = "创建时间")
      @TableField(fill = FieldFill.INSERT)
    private Long createTime;

    @ApiModelProperty(value = "创建人")
      @TableField(fill = FieldFill.INSERT)
    private Long createUserId;

    @ApiModelProperty(value = "更新时间")
      @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateTime;

    @ApiModelProperty(value = "更新人")
      @TableField(fill = FieldFill.INSERT_UPDATE)
    private Integer updateUserId;

    @ApiModelProperty(value = "删除状态 1：删除 2：正常")
      @TableField(fill = FieldFill.INSERT)
    @TableLogic
    private Integer deleted;


    public static final String ID = "id";

    public static final String FLOW_ID = "flow_id";

    public static final String VIEW_ID = "view_id";

    public static final String NAME = "name";

    public static final String CONDITION_JSON = "condition_json";

    public static final String CREATE_TIME = "create_time";

    public static final String CREATE_USER_ID = "create_user_id";

    public static final String UPDATE_TIME = "update_time";

    public static final String UPDATE_USER_ID = "update_user_id";

    public static final String DELETED = "deleted";

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
