package com.zy123.affair.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 应用表单视图
 * </p>
 *
 * @author lishutao
 * @since 2022-11-08
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("affair_app_view")
@ApiModel(value="AppView对象", description="应用表单视图")
public class AppView extends Model<AppView> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
      private Long id;

    @ApiModelProperty(value = "表单Id")
    private Long formId;

    @ApiModelProperty(value = "应用菜单Id")
    private Long appMenuId;

    @ApiModelProperty(value = "页面视图模型")
    private String modelJson;

    @ApiModelProperty(value = "创建时间")
      @TableField(fill = FieldFill.INSERT)
    private Long createTime;

    @ApiModelProperty(value = "创建人")
      @TableField(fill = FieldFill.INSERT)
    private Long createUserId;

    @ApiModelProperty(value = "更新时间")
      @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateTime;

    @ApiModelProperty(value = "更新人")
      @TableField(fill = FieldFill.INSERT_UPDATE)
    private Integer updateUserId;

    @ApiModelProperty(value = "删除状态 1：删除 2：正常")
      @TableField(fill = FieldFill.INSERT)
    @TableLogic
    private Integer deleted;


    public static final String ID = "id";

    public static final String FORM_ID = "form_id";

    public static final String APP_MENU_ID = "app_menu_id";

    public static final String MODEL_JSON = "model_json";

    public static final String CREATE_TIME = "create_time";

    public static final String CREATE_USER_ID = "create_user_id";

    public static final String UPDATE_TIME = "update_time";

    public static final String UPDATE_USER_ID = "update_user_id";

    public static final String DELETED = "deleted";

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
