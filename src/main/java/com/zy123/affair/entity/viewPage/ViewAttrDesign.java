package com.zy123.affair.entity.viewPage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author list
 */
@Data
@ApiModel(value="属性设置的属性信息", description="")
public class ViewAttrDesign {

    @ApiModelProperty(value = "使用位置 1. 表单属性 2. 查询属性 3. 默认操作属性 4. 表格列属性")
    private String useType;

    @ApiModelProperty(value = "表单属性Id")
    private Long formAttrId;

    @ApiModelProperty(value = "名字")
    private Long name;

    @ApiModelProperty(value = "拼音名称")
    private String pyName;

    @ApiModelProperty(value = "属性值")
    private String value;

    @ApiModelProperty(value = "查询条件")
    private String condition;

    @ApiModelProperty(value = "显示类型")
    private Integer showType;
}
