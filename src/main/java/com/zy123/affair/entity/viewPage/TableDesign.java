package com.zy123.affair.entity.viewPage;

import lombok.Data;

import java.util.List;
/**
 * @author list
 */
@Data
public class TableDesign {
   private List<ViewAttrDesign> columnDesignerList;
   private SearchDesign searchDesign;
   private List<ViewAttrDesign> columnButtonDesignerList;
}
