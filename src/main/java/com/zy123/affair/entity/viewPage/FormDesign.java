package com.zy123.affair.entity.viewPage;

import lombok.Data;

import java.util.List;
/**
 * @author list
 */
@Data
public class FormDesign {
    private List<ViewAttrDesign> attrDesignList;
}
