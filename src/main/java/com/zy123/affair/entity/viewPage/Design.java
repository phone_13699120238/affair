package com.zy123.affair.entity.viewPage;

import lombok.Data;

import java.util.List;

/**
 * @author list
 */
@Data
public class Design {
    private TableDesign tableDesign;
    private FormDesign formDesign;
    private List<ViewAttrDesign> buttonDesignerList;
}
