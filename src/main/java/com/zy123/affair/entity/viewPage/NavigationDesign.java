package com.zy123.affair.entity.viewPage;

import lombok.Data;

@Data
public class NavigationDesign extends ViewAttrDesign{
    private Boolean enable;
    private Integer width;
}
