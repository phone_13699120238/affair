package com.zy123.affair.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 表单属性
 * </p>
 *
 * @author lishutao
 * @since 2022-11-08
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("affair_form_data")
@ApiModel(value="FormData对象", description="表单属性")
public class FormData extends Model<FormData> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
      private Long id;

    @ApiModelProperty(value = "公司id")
    private Long companyId;

    @ApiModelProperty(value = "表单")
    private Long formId;

    @ApiModelProperty(value = "实际json数据")
    private String dataJson;

    @ApiModelProperty(value = "创建时间")
      @TableField(fill = FieldFill.INSERT)
    private Long createTime;

    @ApiModelProperty(value = "创建人")
      @TableField(fill = FieldFill.INSERT)
    private Long createUserId;

    @ApiModelProperty(value = "更新时间")
      @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateTime;

    @ApiModelProperty(value = "更新人")
      @TableField(fill = FieldFill.INSERT_UPDATE)
    private Integer updateUserId;

    @ApiModelProperty(value = "删除状态 1：删除 2：正常")
      @TableField(fill = FieldFill.INSERT)
    @TableLogic
    private Integer deleted;

    private String str1;

    private String str2;

    private String str3;

    private String str4;

    private String str5;

    private String str6;

    private String str7;

    private String str8;

    private String str9;

    private String str10;

    private BigDecimal number1;

    private BigDecimal number2;

    private BigDecimal number3;

    private BigDecimal number4;

    private BigDecimal number5;


    public static final String ID = "id";

    public static final String COMPANY_ID = "company_id";

    public static final String FORM_ID = "form_id";

    public static final String DATA_JSON = "data_json";

    public static final String CREATE_TIME = "create_time";

    public static final String CREATE_USER_ID = "create_user_id";

    public static final String UPDATE_TIME = "update_time";

    public static final String UPDATE_USER_ID = "update_user_id";

    public static final String DELETED = "deleted";

    public static final String STR1 = "str1";

    public static final String STR2 = "str2";

    public static final String STR3 = "str3";

    public static final String STR4 = "str4";

    public static final String STR5 = "str5";

    public static final String STR6 = "str6";

    public static final String STR7 = "str7";

    public static final String STR8 = "str8";

    public static final String STR9 = "str9";

    public static final String STR10 = "str10";

    public static final String NUMBER1 = "number1";

    public static final String NUMBER2 = "number2";

    public static final String NUMBER3 = "number3";

    public static final String NUMBER4 = "number4";

    public static final String NUMBER5 = "number5";

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
