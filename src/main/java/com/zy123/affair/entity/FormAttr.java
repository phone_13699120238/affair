package com.zy123.affair.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 表单数据
 * </p>
 *
 * @author lishutao
 * @since 2022-11-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("affair_form_attr")
@ApiModel(value="FormAttr对象", description="表单数据")
public class FormAttr extends Model<FormAttr> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
      private Long id;

    @ApiModelProperty(value = "公司id")
    private Long companyId;

    @ApiModelProperty(value = "表单")
    private Long formId;

    @ApiModelProperty(value = "名字")
    private String name;

    @ApiModelProperty(value = "拼音名称")
    private String pyName;

    @ApiModelProperty(value = "固定属性状态：1: 可操作  21：用户Id 22：用户名称  31：父节点Id 32：节点名称")
    private Integer fixed;

    @ApiModelProperty(value = "类型 1:文本 2：整数  3：日期  4：小数  5：选择 6：表单 ")
    private String type;

    @ApiModelProperty(value = "类型： 1： 单值  2：多值")
    private Integer valueCountType;

    @ApiModelProperty(value = "范围值")
    private String valueScope;

    @ApiModelProperty(value = "显示属性；type = 6 时,")
    private String showAttr;

    @ApiModelProperty(value = "多值连接符号，value_cout_type = 2 时")
    private String linkChar;

    @ApiModelProperty(value = "创建时间")
      @TableField(fill = FieldFill.INSERT)
    private Long createTime;

    @ApiModelProperty(value = "创建人")
      @TableField(fill = FieldFill.INSERT)
    private Long createUserId;

    @ApiModelProperty(value = "更新时间")
      @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateTime;

    @ApiModelProperty(value = "更新人")
      @TableField(fill = FieldFill.INSERT_UPDATE)
    private Integer updateUserId;

    @ApiModelProperty(value = "删除状态 1：删除 2：正常")
      @TableField(fill = FieldFill.INSERT)
    @TableLogic
    private Integer deleted;


    public static final String ID = "id";

    public static final String COMPANY_ID = "company_id";

    public static final String FORM_ID = "form_id";

    public static final String NAME = "name";

    public static final String PY_NAME = "py_name";

    public static final String FIXED = "fixed";

    public static final String TYPE = "type";

    public static final String VALUE_COUNT_TYPE = "value_count_type";

    public static final String VALUE_SCOPE = "value_scope";

    public static final String SHOW_ATTR = "show_attr";

    public static final String LINK_CHAR = "link_char";

    public static final String CREATE_TIME = "create_time";

    public static final String CREATE_USER_ID = "create_user_id";

    public static final String UPDATE_TIME = "update_time";

    public static final String UPDATE_USER_ID = "update_user_id";

    public static final String DELETED = "deleted";

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
