package com.zy123.affair.entity.formAttr;

import lombok.Data;

@Data
public class FormAttrSelect {
    private Integer key;
    private String name;
}
