package com.zy123.affair.entity.formAttr;

import lombok.Data;

import java.util.List;

@Data
public class FormAttrValueScope {
    /***表单格式***/
    private Long formId;
    private List<FormSearch>  searchList;

    /***选择框格式***/
    private List<FormAttrSelect> selectList;
}
