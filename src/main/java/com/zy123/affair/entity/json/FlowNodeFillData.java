package com.zy123.affair.entity.json;

import io.swagger.annotations.ApiModel;

import java.util.List;

@ApiModel(value="流程节点填充对象", description="流程节点填充")
public class FlowNodeFillData {
    // 1: 添加 2：填充
    private Integer optType;
    private Long formId;
    private List<String> attrList;

    private List<ShowView> showViewList;
}
