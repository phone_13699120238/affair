package com.zy123.affair.entity.json;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value="流程权限配置对象", description="流程权限配置")
public class FlowPermission {

    @ApiModelProperty(value = "1： 任何人 2： 部门 3： 特定人员")
    private Integer runType;

    //TODO runValue  选择不同类型时，对应的值； 同时计算登录用户的权限（是否问题？）
    private String runValue;

    @ApiModelProperty(value = "1： 任何人 2： 部门  3： 特定人员 4. 流程参与者 ")
    private String visitType;
    //TODO  登录用户符合1,2,3 权限所以数据任何查看  登录用户符合4, 查看自己参与的流程
    private String visiteValue;


    private boolean checkRun(Long userId){
        return true;
    }



}
