package com.zy123.affair.entity.json;

public class FlowNodeFinish {
    //1. 任意人员 2：全部完成 3. 比例策略
    private Integer type;

    //策略条件， 1. 完成比例 2：完成梳理
    private String ceLue;

    //1. 不过期 2：持续时间 3. 特定时间
    private Integer overdueType;
    //过期值
    private String overdueValue;
}
