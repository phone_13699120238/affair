package com.zy123.affair.baseenum.formAttr;

/**
 * @author list
 */

public enum FormAttrValueCountTypeEnum {
    /**/
    SINGLE(1,"单值"),
    MULTI(2,"多值");

    private Integer value;
    private String name;
    FormAttrValueCountTypeEnum(Integer value, String name) {
        this.value =value;
        this.name =name;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
