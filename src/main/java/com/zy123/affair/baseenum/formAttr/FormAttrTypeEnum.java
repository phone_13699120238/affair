package com.zy123.affair.baseenum.formAttr;

/**
 * @author list
 */

public enum FormAttrTypeEnum {
    /**/
    SELECT(5,"选择"),
    FORM(6,"表单");

    private Integer value;
    private String name;
    FormAttrTypeEnum(Integer value, String name) {
        this.value =value;
        this.name =name;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
