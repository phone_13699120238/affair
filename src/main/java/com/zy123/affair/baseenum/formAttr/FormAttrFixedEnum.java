package com.zy123.affair.baseenum.formAttr;

/**
 * @author list
 */

public enum FormAttrFixedEnum {
    /**/
    NOFIXED(1,"可编辑"),
    USERID(21,"用户Id"),
    USERNAME(22,"用户名称"),
    PARENTID(31,"父节点Id"),
    NODENAME(32,"节点名称");

    private Integer value;
    private String name;
    FormAttrFixedEnum(Integer value, String name) {
        this.value =value;
        this.name =name;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
