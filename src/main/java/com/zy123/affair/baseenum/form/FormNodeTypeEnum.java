package com.zy123.affair.baseenum.form;

/**
 * @author list
 */

public enum FormNodeTypeEnum {
    /**/
    TYPE(1,"分类"),
    BUSSINESS(2,"业务数据"),
    User(3,"用户数据"),
    Tree(4,"目录数据");

    private Integer value;
    private String name;
    FormNodeTypeEnum(Integer value, String name) {
        this.value =value;
        this.name =name;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
