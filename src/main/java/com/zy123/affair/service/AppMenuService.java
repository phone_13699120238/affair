package com.zy123.affair.service;

import com.zy123.affair.common.baseEntity.ResultObj;
import com.zy123.affair.dto.AppDto;
import com.zy123.affair.dto.AppMenuDto;
import com.zy123.affair.dto.TreeNodeDto;
import com.zy123.affair.entity.AppMenu;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * <p>
 * 应用页面导航菜单 服务类
 * </p>
 *
 * @author lishutao
 * @since 2022-09-27
 */
public interface AppMenuService extends IService<AppMenu> {
    /**
     * 获取完整树
     */
    List<TreeNodeDto> tree(Long  companyId);


    public void addNode(Long appId,TreeNodeDto node);


    public void editNode(TreeNodeDto node);

    /**
     * 删除
     * @param idList
     */
    public void removeNode(Long appId,List<Long> idList);

    /**
     * 获取全路径
     * @param id
     * @return
     */
    public List<TreeNodeDto> getFullPath(Long appId,Long id);


    /**
     * 获取所有子节点
     */
    public List<TreeNodeDto> listSubNode(Long appId,Long id);


    public void  edit(AppMenuDto dto);

    AppMenuDto get(Long id);
}
