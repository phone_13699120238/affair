package com.zy123.affair.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zy123.affair.dto.TreeNodeDto;
import com.zy123.affair.entity.Form;

import java.util.List;

/**
 * <p>
 * 事务 服务类
 * </p>
 *
 * @author lishutao
 * @since 2022-09-27
 */
public interface FormService extends IService<Form> {
    /**
     * 获取完整树
     */
    List<TreeNodeDto> tree(Long companyId);


    public void addNode(Long companyId,TreeNodeDto node);


    public void editNode(Long companyId,TreeNodeDto node);

    /**
     * 删除
     * @param idList
     */
    public void removeNode(Long companyId,List<Long> idList);

    /**
     * 获取全路径
     * @param id
     * @return
     */
    public List<TreeNodeDto> getFullPath(Long companyId,Long id);


    /**
     * 获取所有子节点
     */
    public List<TreeNodeDto> listSubNode(Long companyId,Long id);
}
