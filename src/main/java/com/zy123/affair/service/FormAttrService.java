package com.zy123.affair.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zy123.affair.common.baseEntity.SwaggerPage;
import com.zy123.affair.dto.FormAttrDto;
import com.zy123.affair.dto.FormAttrSearchDto;
import com.zy123.affair.entity.FormAttr;

import java.util.List;

/**
 * <p>
 * 表单数据 服务类
 * </p>
 *
 * @author lishutao
 * @since 2022-09-27
 */
public interface FormAttrService extends IService<FormAttr> {
    SwaggerPage<FormAttrDto> search(Long companyId,FormAttrSearchDto searchDto);

    void add(Long companyId,FormAttrDto dto);

    void edit(Long companyId,FormAttrDto dto);

    void remove(Long companyId,List<Long> idList);

    FormAttrDto get(Long companyId,Long id);

    List<FormAttrDto> listByIdList(Long companyId,List<Long> idList);

    List<FormAttrDto> list(Long companyId,FormAttrSearchDto searchDto);


    //TODO 需要追加缓存
    List<FormAttr> listByFormId(Long formId);
}
