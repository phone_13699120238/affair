package com.zy123.affair.service;

import com.zy123.affair.common.baseEntity.SwaggerPage;
import com.zy123.affair.dto.FlowDataDto;
import com.zy123.affair.dto.FlowDataSearchDto;
import com.zy123.affair.entity.FlowData;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 流程节点数据 服务类
 * </p>
 *
 * @author lishutao
 * @since 2022-11-07
 */
public interface FlowDataService extends IService<FlowData> {
    SwaggerPage<FlowDataDto> search(Long companyId, FlowDataSearchDto searchDto);

    void add(Long companyId,FlowDataDto dto);

    void edit(Long companyId,FlowDataDto dto);

    void remove(Long companyId, List<Long> idList);

    FlowDataDto get(Long companyId,Long id);

    List<FlowDataDto> listByIdList(Long companyId,List<Long> idList);

    List<FlowDataDto> list(Long companyId,FlowDataSearchDto searchDto);
}
