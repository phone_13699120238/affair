package com.zy123.affair.service;

import com.zy123.affair.common.baseEntity.SwaggerPage;
import com.zy123.affair.dto.FlowDto;
import com.zy123.affair.dto.FlowSearchDto;
import com.zy123.affair.entity.Flow;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 流程模板 服务类
 * </p>
 *
 * @author lishutao
 * @since 2022-11-07
 */
public interface FlowService extends IService<Flow> {
    SwaggerPage<FlowDto> search(Long companyId, FlowSearchDto searchDto);

    void add(Long companyId,FlowDto dto);

    void edit(Long companyId,FlowDto dto);

    void remove(Long companyId, List<Long> idList);

    FlowDto get(Long companyId,Long id);

    List<FlowDto> listByIdList(Long companyId,List<Long> idList);

    List<FlowDto> list(Long companyId,FlowSearchDto searchDto);
}
