package com.zy123.affair.service;

import com.zy123.affair.common.baseEntity.SwaggerPage;
import com.zy123.affair.dto.FlowNodeDto;
import com.zy123.affair.dto.FlowNodeSearchDto;
import com.zy123.affair.entity.FlowNode;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 流程模板节点 服务类
 * </p>
 *
 * @author lishutao
 * @since 2022-11-07
 */
public interface FlowNodeService extends IService<FlowNode> {
    SwaggerPage<FlowNodeDto> search(Long companyId, FlowNodeSearchDto searchDto);

    void add(Long companyId,FlowNodeDto dto);

    void edit(Long companyId,FlowNodeDto dto);

    void remove(Long companyId, List<Long> idList);

    FlowNodeDto get(Long companyId,Long id);

    List<FlowNodeDto> listByIdList(Long companyId,List<Long> idList);

    List<FlowNodeDto> list(Long companyId,FlowNodeSearchDto searchDto);
}
