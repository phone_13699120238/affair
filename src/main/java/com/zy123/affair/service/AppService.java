package com.zy123.affair.service;

import com.zy123.affair.common.baseEntity.SwaggerPage;
import com.zy123.affair.dto.AppDto;
import com.zy123.affair.dto.AppSearchDto;
import com.zy123.affair.entity.App;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 页面 服务类
 * </p>
 *
 * @author lishutao
 * @since 2022-09-27
 */
public interface AppService extends IService<App> {
    SwaggerPage<AppDto> search(AppSearchDto searchDto);

    void add(AppDto dto);

    void edit(AppDto dto);

    void remove(List<Long> idList);

    AppDto get(Long id);

    List<AppDto> listByIdList(List<Long> idList);

    List<AppDto> list(AppSearchDto searchDto);
}
