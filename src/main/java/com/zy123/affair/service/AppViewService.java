package com.zy123.affair.service;

import com.zy123.affair.common.baseEntity.ResultObj;
import com.zy123.affair.common.baseEntity.SwaggerPage;
import com.zy123.affair.dto.AppDto;
import com.zy123.affair.dto.AppSearchDto;
import com.zy123.affair.dto.AppViewDto;
import com.zy123.affair.dto.AppViewSearchDto;
import com.zy123.affair.entity.AppView;
import com.baomidou.mybatisplus.extension.service.IService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lishutao
 * @since 2022-09-27
 */
public interface AppViewService extends IService<AppView> {

    AppViewDto get(Long id);

    AppViewDto getModel(Long id);

    void save(AppViewDto dto);

    void saveModel(AppViewDto dto);
}
