package com.zy123.affair.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zy123.affair.common.BeanConverter;
import com.zy123.affair.common.baseEntity.SwaggerPage;
import com.zy123.affair.dto.FlowDto;
import com.zy123.affair.dto.FlowSearchDto;
import com.zy123.affair.entity.Flow;
import com.zy123.affair.entity.Flow;
import com.zy123.affair.mapper.FlowMapper;
import com.zy123.affair.service.FlowService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 流程模板 服务实现类
 * </p>
 *
 * @author lishutao
 * @since 2022-11-07
 */
@Service
public class FlowServiceImpl extends ServiceImpl<FlowMapper, Flow> implements FlowService {
    @Override
    public SwaggerPage<FlowDto> search(Long companyId, FlowSearchDto searchDto) {
        Page<Flow> page = new Page<>(searchDto.getPageNum(),searchDto.getPageSize());
        QueryWrapper<Flow> queryWrapper = getWrapperBySearch(companyId,searchDto);
        this.page(page,queryWrapper);

        List<FlowDto> dtoList = dbToDto(page.getRecords());

        SwaggerPage<FlowDto> resultPage = BeanConverter.map(searchDto, SwaggerPage.class);
        resultPage.setList(dtoList);
        resultPage.setTotal((int)page.getTotal());
        return resultPage;
    }

    @Override
    public void add(Long companyId,FlowDto dto) {
        Flow mainEntity= BeanConverter.map(dto, Flow.class);
     //   mainEntity.setCompanyId(companyId);
        mainEntity.insert();
    }

    @Override
    public void edit(Long companyId,FlowDto dto) {
        Flow mainEntity= BeanConverter.map(dto, Flow.class);
     //   mainEntity.setCompanyId(companyId);
        mainEntity.updateById();
    }

    @Override
    public void remove(Long companyId,List<Long> idList) {
        this.removeByIds(idList);
    }

    @Override
    public FlowDto get(Long companyId,Long id) {
        Flow entity = this.getById(id);
        return BeanConverter.map(entity,FlowDto.class);
    }

    @Override
    public List<FlowDto> listByIdList(Long companyId,List<Long> idList) {
        List<Flow> dbList = this.listByIds(idList);
        return BeanConverter.asList(dbList,FlowDto.class);
    }

    @Override
    public List<FlowDto> list(Long companyId,FlowSearchDto searchDto) {
        QueryWrapper<Flow> queryWrapper = getWrapperBySearch(companyId,searchDto);
        List<Flow> list = this.list(queryWrapper);
        List<FlowDto> dtoList = dbToDto(list);
        return dtoList;
    }




    private QueryWrapper<Flow> getWrapperBySearch(Long companyId,FlowSearchDto searchDto) {
        Flow queryEntity=new Flow();
        QueryWrapper<Flow> queryWrapper = new QueryWrapper<>(queryEntity);
     //   queryWrapper.eq(Flow.COMPANY_ID,companyId);
        queryWrapper.eq(Flow.FORM_ID,searchDto.getFormId());

        if(CollectionUtils.isEmpty(searchDto.getOrderList())){
            queryWrapper.orderByDesc(Flow.CREATE_TIME);
        }

        return queryWrapper;
    }

    private List<FlowDto> dbToDto(List<Flow> list) {
        List<FlowDto> dtoList=new ArrayList<>();
        for(Flow tmp : list){
            FlowDto tmpDto = BeanConverter.map(tmp, FlowDto.class);
            dtoList.add(tmpDto);
        }
        return dtoList;
    }
}
