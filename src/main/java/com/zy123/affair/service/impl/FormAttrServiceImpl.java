package com.zy123.affair.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zy123.affair.baseenum.formAttr.FormAttrFixedEnum;
import com.zy123.affair.common.BeanConverter;
import com.zy123.affair.common.PinyinUtil;
import com.zy123.affair.common.baseEntity.SwaggerPage;
import com.zy123.affair.dto.FormAttrDto;
import com.zy123.affair.dto.FormAttrSearchDto;
import com.zy123.affair.entity.FormAttr;
import com.zy123.affair.entity.FormAttr;
import com.zy123.affair.mapper.FormAttrMapper;
import com.zy123.affair.service.FormAttrService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 表单数据 服务实现类
 * </p>
 *
 * @author lishutao
 * @since 2022-09-27
 */
@Service
public class FormAttrServiceImpl extends ServiceImpl<FormAttrMapper, FormAttr> implements FormAttrService {
    @Override
    public SwaggerPage<FormAttrDto> search(Long companyId,FormAttrSearchDto searchDto) {
        Page<FormAttr> page = new Page<>(searchDto.getPageNum(),searchDto.getPageSize());
        QueryWrapper<FormAttr> queryWrapper = getWrapperBySearch(companyId,searchDto);
        this.page(page,queryWrapper);

        List<FormAttrDto> dtoList = dbToDto(page.getRecords());

        SwaggerPage<FormAttrDto> resultPage = BeanConverter.map(searchDto, SwaggerPage.class);
        resultPage.setList(dtoList);
        resultPage.setTotal((int)page.getTotal());
        return resultPage;
    }

    @Override
    public void add(Long companyId,FormAttrDto dto) {
        String pinyin = PinyinUtil.getPinyin(dto.getName());
        dto.setPyName(pinyin);
        dto.setFixed(FormAttrFixedEnum.NOFIXED.getValue());
        FormAttr mainEntity= BeanConverter.map(dto, FormAttr.class);
        mainEntity.setCompanyId(companyId);
        mainEntity.insert();
    }

    @Override
    public void edit(Long companyId,FormAttrDto dto) {
        String pinyin = PinyinUtil.getPinyin(dto.getName());
        dto.setPyName(pinyin);
        FormAttr mainEntity= BeanConverter.map(dto, FormAttr.class);
        mainEntity.setCompanyId(companyId);
        mainEntity.updateById();
    }

    @Override
    public void remove(Long companyId,List<Long> idList) {
        this.removeByIds(idList);
    }

    @Override
    public FormAttrDto get(Long companyId,Long id) {
        FormAttr entity = this.getById(id);
        return BeanConverter.map(entity,FormAttrDto.class);
    }

    @Override
    public List<FormAttrDto> listByIdList(Long companyId,List<Long> idList) {
        List<FormAttr> dbList = this.listByIds(idList);
        return BeanConverter.asList(dbList,FormAttrDto.class);
    }

    @Override
    public List<FormAttrDto> list(Long companyId,FormAttrSearchDto searchDto) {
        QueryWrapper<FormAttr> queryWrapper = getWrapperBySearch(companyId,searchDto);
        List<FormAttr> list = this.list(queryWrapper);
        List<FormAttrDto> dtoList = dbToDto(list);
        return dtoList;
    }

    @Override
    public List<FormAttr> listByFormId(Long formId) {
        QueryWrapper<FormAttr> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(FormAttr.FORM_ID,formId);
        return this.list(queryWrapper);
    }


    private QueryWrapper<FormAttr> getWrapperBySearch(Long companyId,FormAttrSearchDto searchDto) {
        FormAttr queryEntity=new FormAttr();
        QueryWrapper<FormAttr> queryWrapper = new QueryWrapper<>(queryEntity);
        queryWrapper.eq(FormAttr.COMPANY_ID,companyId);
        queryWrapper.eq(FormAttr.FORM_ID,searchDto.getFormId());

        if(CollectionUtils.isEmpty(searchDto.getOrderList())){
            queryWrapper.orderByDesc(FormAttr.CREATE_TIME);
        }

        return queryWrapper;
    }

    private List<FormAttrDto> dbToDto(List<FormAttr> list) {
        List<FormAttrDto> dtoList=new ArrayList<>();
        for(FormAttr tmp : list){
            FormAttrDto tmpDto = BeanConverter.map(tmp, FormAttrDto.class);
            dtoList.add(tmpDto);
        }
        return dtoList;
    }
}
