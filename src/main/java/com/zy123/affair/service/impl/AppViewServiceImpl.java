package com.zy123.affair.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zy123.affair.common.BeanConverter;
import com.zy123.affair.common.baseEntity.SwaggerPage;
import com.zy123.affair.common.jackson.JsonUtils;
import com.zy123.affair.dto.AppViewDto;
import com.zy123.affair.dto.AppViewSearchDto;
import com.zy123.affair.entity.AppView;
import com.zy123.affair.entity.AppView;
import com.zy123.affair.entity.x6.SceneViewCell;
import com.zy123.affair.entity.x6.SceneViewModel;
import com.zy123.affair.entity.x6.ViewPageNode;
import com.zy123.affair.mapper.AppViewMapper;
import com.zy123.affair.service.AppViewService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zy123.affair.service.ViewPageService;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lishutao
 * @since 2022-09-27
 */
@Service
public class AppViewServiceImpl extends ServiceImpl<AppViewMapper, AppView> implements AppViewService {
    @Autowired
    private ViewPageService viewPageService;

    @Override
    public AppViewDto get(Long id) {
        AppView entity = this.getById(id);
        if(entity == null){
            return null;
        }
        entity.setModelJson(null);
        return BeanConverter.map(entity,AppViewDto.class);
    }

    @Override
    public AppViewDto getModel(Long id) {
        AppView entity = this.getById(id);
        if(entity == null){
            return null;
        }
        AppViewDto dto = BeanConverter.map(entity, AppViewDto.class);
        return dto;
    }

    @Override
    public void save(AppViewDto dto) {
        AppView dbEntity = this.getById(dto.getAppMenuId());
        if(dbEntity == null){
            AppView mainEntity= BeanConverter.map(dto, AppView.class);
            mainEntity.setId(dto.getAppMenuId());
            mainEntity.insert();
        }else{
            AppView mainEntity= BeanConverter.map(dto, AppView.class);
            mainEntity.setId(dto.getAppMenuId());
            mainEntity.updateById();
        }
    }

    @Override
    public void saveModel(AppViewDto dto) {
        AppView dbEntity = this.getById(dto.getAppMenuId());
        AppView mainEntity= BeanConverter.map(dto, AppView.class);
        mainEntity.setId(dto.getAppMenuId());
        if(dbEntity == null){
            mainEntity.insert();
        }else{
            mainEntity.updateById();
        }

        //页面表数据分解
        SceneViewModel<ViewPageNode> sceneViewModel = JsonUtils.toObject(dto.getModelJson(), SceneViewModel.class);
        List<SceneViewCell<ViewPageNode>> viewCells = sceneViewModel.getCells();
        List<SceneViewCell<ViewPageNode>> nodeList = viewCells.stream()
                .filter(e -> e.getShape().endsWith("Page")).collect(Collectors.toList());

        viewPageService.resetByFlow(nodeList,dto.getAppMenuId());

    }




    private QueryWrapper<AppView> getWrapperBySearch(AppViewSearchDto searchDto) {
        AppView queryEntity=new AppView();

        QueryWrapper<AppView> queryWrapper = new QueryWrapper<>(queryEntity);

        if(CollectionUtils.isEmpty(searchDto.getOrderList())){
           // queryWrapper.orderByDesc(AppView.CREATE_TIME);
        }

        return queryWrapper;
    }

    private List<AppViewDto> dbToDto(List<AppView> list) {
        List<AppViewDto> dtoList=new ArrayList<>();
        for(AppView tmp : list){
            AppViewDto tmpDto = BeanConverter.map(tmp, AppViewDto.class);
            dtoList.add(tmpDto);
        }
        return dtoList;
    }
}
