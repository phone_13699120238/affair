package com.zy123.affair.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zy123.affair.common.BeanConverter;
import com.zy123.affair.common.baseEntity.SwaggerPage;
import com.zy123.affair.dto.AppDto;
import com.zy123.affair.dto.AppSearchDto;
import com.zy123.affair.entity.App;
import com.zy123.affair.mapper.AppMapper;
import com.zy123.affair.service.AppService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.collections4.CollectionUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 页面 服务实现类
 * </p>
 *
 * @author lishutao
 * @since 2022-09-27
 */
@Service
public class AppServiceImpl extends ServiceImpl<AppMapper, App> implements AppService {

    @Override
    public SwaggerPage<AppDto> search(AppSearchDto searchDto) {
        Page<App> page = new Page<>(searchDto.getPageNum(),searchDto.getPageSize());
        QueryWrapper<App> queryWrapper = getWrapperBySearch(searchDto);
        this.page(page,queryWrapper);

        List<AppDto> dtoList = dbToDto(page.getRecords());

        SwaggerPage<AppDto> resultPage = BeanConverter.map(searchDto, SwaggerPage.class);
        resultPage.setList(dtoList);
        resultPage.setTotal((int)page.getTotal());
        return resultPage;
    }


    @Override
    public void add(AppDto dto) {
        App mainEntity= BeanConverter.map(dto, App.class);
        mainEntity.insert();
    }

    @Override
    public void edit(AppDto dto) {
        App mainEntity= BeanConverter.map(dto, App.class);
        mainEntity.updateById();
    }

    @Override
    public void remove(List<Long> idList) {
        this.removeByIds(idList);
    }

    @Override
    public AppDto get(Long id) {
        App entity = this.getById(id);
        return BeanConverter.map(entity,AppDto.class);
    }

    @Override
    public List<AppDto> listByIdList(List<Long> idList) {
        List<App> dbList = this.listByIds(idList);
        return BeanConverter.asList(dbList,AppDto.class);
    }

    @Override
    public List<AppDto> list(AppSearchDto searchDto) {
        QueryWrapper<App> queryWrapper = getWrapperBySearch(searchDto);
        List<App> list = this.list(queryWrapper);
        List<AppDto> dtoList = dbToDto(list);
        return dtoList;
    }


    private QueryWrapper<App> getWrapperBySearch(AppSearchDto searchDto) {
        App queryEntity=new App();

        QueryWrapper<App> queryWrapper = new QueryWrapper<>(queryEntity);

        if(CollectionUtils.isEmpty(searchDto.getOrderList())){
            queryWrapper.orderByDesc(App.CREATE_TIME);
        }

        return queryWrapper;
    }

    private List<AppDto> dbToDto(List<App> list) {
        List<AppDto> dtoList=new ArrayList<>();
        for(App tmp : list){
            AppDto tmpDto = BeanConverter.map(tmp, AppDto.class);
            dtoList.add(tmpDto);
        }
        return dtoList;
    }
}
