package com.zy123.affair.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zy123.affair.baseenum.form.FormNodeTypeEnum;
import com.zy123.affair.baseenum.formAttr.FormAttrFixedEnum;
import com.zy123.affair.common.BeanConverter;
import com.zy123.affair.common.ListTreeUtil;
import com.zy123.affair.dto.TreeNodeDto;
import com.zy123.affair.entity.Form;
import com.zy123.affair.entity.FormAttr;
import com.zy123.affair.mapper.FormMapper;
import com.zy123.affair.service.FormAttrService;
import com.zy123.affair.service.FormService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * <p>
 * 事务 服务实现类
 * </p>
 *
 * @author lishutao
 * @since 2022-09-27
 */
@Service
public class FormServiceImpl extends ServiceImpl<FormMapper, Form> implements FormService {
    /**
     * 获取完整树
     *
     * @param companyId
     */
    @Override
    public List<TreeNodeDto> tree(Long companyId) {
        QueryWrapper queryWrapper=new QueryWrapper();
        queryWrapper.eq(Form.COMPANY_ID,companyId);
        List<Form> list = this.list(queryWrapper);

        List<TreeNodeDto> dtoList = BeanConverter.asList(list, TreeNodeDto.class);
        List<TreeNodeDto> treeList = ListTreeUtil.listToTree(dtoList, -1L);
        return treeList;
    }

    @Override
    public void addNode(Long companyId,TreeNodeDto node) {
        Form entity=BeanConverter.map(node,Form.class);
        entity.setCompanyId(companyId);
        this.save(entity);

        //为用户数据添加固定属性
        if(FormNodeTypeEnum.User.getValue().equals(node.getNodeType())){
            FormAttr userTempFormAttr1=new FormAttr();
            userTempFormAttr1.setName("账号Id");
            userTempFormAttr1.setPyName("userId");
            userTempFormAttr1.setFixed(FormAttrFixedEnum.USERID.getValue());
            userTempFormAttr1.setCompanyId(entity.getCompanyId());
            userTempFormAttr1.setFormId(entity.getId());
            userTempFormAttr1.insert();

            FormAttr userTempFormAttr2=new FormAttr();
            userTempFormAttr2.setName("姓名");
            userTempFormAttr2.setPyName("name");
            userTempFormAttr2.setFixed(FormAttrFixedEnum.USERNAME.getValue());
            userTempFormAttr2.setCompanyId(entity.getCompanyId());
            userTempFormAttr2.setFormId(entity.getId());
            userTempFormAttr2.insert();
            return;
        }

        //为目录数据添加固定属性
        if(FormNodeTypeEnum.Tree.getValue().equals(node.getNodeType())){
            FormAttr treeTempFormAttr1=new FormAttr();
            treeTempFormAttr1.setName("父节点Id");
            treeTempFormAttr1.setPyName("parentId");
            treeTempFormAttr1.setFixed(FormAttrFixedEnum.PARENTID.getValue());
            treeTempFormAttr1.setCompanyId(entity.getCompanyId());
            treeTempFormAttr1.setFormId(entity.getId());
            treeTempFormAttr1.insert();

            FormAttr treeTempFormAttr2=new FormAttr();
            treeTempFormAttr2.setName("节点名称");
            treeTempFormAttr2.setPyName("name");
            treeTempFormAttr1.setFixed(FormAttrFixedEnum.NODENAME.getValue());
            treeTempFormAttr2.setCompanyId(entity.getCompanyId());
            treeTempFormAttr2.setFormId(entity.getId());
            treeTempFormAttr2.insert();
           return;
        }
    }

    @Override
    public void editNode(Long companyId,TreeNodeDto node) {
        Form entity=BeanConverter.map(node,Form.class);
        this.updateById(entity);
    }

    /**
     * 删除
     *
     * @param idList
     */
    @Override
    public void removeNode(Long companyId,List<Long> idList) {
        QueryWrapper queryWrapper=new QueryWrapper();
        queryWrapper.eq(Form.COMPANY_ID,companyId);
        List<Form> dbList = this.list(queryWrapper);

        List<Form>  subList=new ArrayList<>();
        for(Long tmpId:idList){
            List<Form> tmpSubList = ListTreeUtil.getFullNode(dbList, tmpId);
            subList.addAll(tmpSubList);
        }
        List<Long> tmpSubIdList = subList.stream().map(e -> e.getId()).collect(Collectors.toList());
        idList.addAll(tmpSubIdList);

        this.removeByIds(idList);
    }

    /**
     * 获取全路径
     *
     * @param id
     * @return
     */
    @Override
    public List<TreeNodeDto> getFullPath(Long companyId,Long id) {
        List<Form> list = this.list();
        QueryWrapper queryWrapper=new QueryWrapper();
        queryWrapper.eq(Form.COMPANY_ID,companyId);
        Map<Long, Form> map = list.stream().collect(Collectors.toMap(e -> e.getId(), Function.identity(), (o1, o2) -> o2));

        List<Form> pathList=new ArrayList<>();
        Long tmpId= id;
        Integer maxLimit=10;
        for(int i=0;i<maxLimit;i++){
            Form tmp = map.get(tmpId);
            pathList.add(0,tmp);
            if(tmp.getParentId().equals(-1L)){
                break;
            }
            tmpId = tmp.getParentId();
        }

        List<TreeNodeDto> dtoList = BeanConverter.asList(pathList, TreeNodeDto.class);
        return dtoList;
    }

    /**
     * 获取所有子节点
     *
     * @param id
     */
    @Override
    public List<TreeNodeDto> listSubNode(Long companyId,Long id) {
        QueryWrapper queryWrapper=new QueryWrapper();
        queryWrapper.eq(Form.COMPANY_ID,companyId);
        List<Form> list = this.list();
        List<Form> fullNodeList = ListTreeUtil.getFullNode(list, id);
        List<TreeNodeDto> dtoList = BeanConverter.asList(fullNodeList, TreeNodeDto.class);
        return dtoList;
    }

}
