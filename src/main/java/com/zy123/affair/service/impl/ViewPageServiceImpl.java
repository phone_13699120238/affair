package com.zy123.affair.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zy123.affair.common.BeanConverter;
import com.zy123.affair.common.baseEntity.SwaggerPage;
import com.zy123.affair.dto.ViewPageDto;
import com.zy123.affair.dto.ViewPageSearchDto;
import com.zy123.affair.entity.ViewPage;
import com.zy123.affair.entity.ViewPage;
import com.zy123.affair.entity.x6.SceneViewCell;
import com.zy123.affair.entity.x6.ViewPageNode;
import com.zy123.affair.mapper.ViewPageMapper;
import com.zy123.affair.service.ViewPageService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 模板页面 服务实现类
 * </p>
 *
 * @author lishutao
 * @since 2022-09-27
 */
@Service
public class ViewPageServiceImpl extends ServiceImpl<ViewPageMapper, ViewPage> implements ViewPageService {

    @Override
    public void edit(ViewPageDto dto) {
        ViewPage mainEntity= BeanConverter.map(dto, ViewPage.class);
        mainEntity.updateById();
    }

    @Override
    public ViewPageDto get(Long id) {
        ViewPage entity = this.getById(id);
        return BeanConverter.map(entity,ViewPageDto.class);
    }

    @Override
    public List<ViewPageDto> listByAppMenuId(Long appMenuId) {
        QueryWrapper<ViewPage> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(ViewPage.APP_VIEW_ID,appMenuId);
        List<ViewPage> list = this.list(queryWrapper);
        List<ViewPageDto> dtoList = dbToDto(list);
        return dtoList;
    }

    /**
     * 重新设置流程页面信息，最大限度减少数据的修改
     * @param nodeList
     * @param appMenuId
     */
    @Override
    public void resetByFlow(List<SceneViewCell<ViewPageNode>> nodeList,Long appMenuId) {
        QueryWrapper<ViewPage> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(ViewPage.APP_VIEW_ID,appMenuId);
        List<ViewPage> list = this.list(queryWrapper);
        Map<String, ViewPage> nodeIdMap = list.stream()
                .collect(Collectors.toMap(e -> e.getNodeKey(), e -> e, (o1, o2) -> o1));

        //存在数据修改、不存在添加； 同时nodeIdMap移除交集数据
        List<ViewPage> saveList=new ArrayList<>(nodeList.size());
        for(SceneViewCell<ViewPageNode> tmp: nodeList){
            ViewPage dbEntity = nodeIdMap.get(tmp.getId());
            ViewPage newEntity= BeanConverter.map(tmp.getData(),ViewPage.class);
            newEntity.setAppViewId(appMenuId);
            newEntity.setNodeKey(tmp.getId());
            if(dbEntity != null){
                newEntity.setId(dbEntity.getId());
                nodeIdMap.remove(tmp.getId());
            }
            saveList.add(newEntity);
        }
        //保存数据
        this.saveOrUpdateBatch(saveList);

        //删除数据
        if(nodeIdMap.size() > 0){
            List<Long> removeIdList = nodeIdMap.values().stream()
                    .map(e -> e.getId()).collect(Collectors.toList());
            this.removeByIds(removeIdList);
        }

    }


    private QueryWrapper<ViewPage> getWrapperBySearch(ViewPageSearchDto searchDto) {
        ViewPage queryEntity=new ViewPage();

        QueryWrapper<ViewPage> queryWrapper = new QueryWrapper<>(queryEntity);

        if(CollectionUtils.isEmpty(searchDto.getOrderList())){
          //  queryWrapper.orderByDesc(ViewPage.CREATE_TIME);
        }

        return queryWrapper;
    }

    private List<ViewPageDto> dbToDto(List<ViewPage> list) {
        List<ViewPageDto> dtoList=new ArrayList<>();
        for(ViewPage tmp : list){
            ViewPageDto tmpDto = BeanConverter.map(tmp, ViewPageDto.class);
            dtoList.add(tmpDto);
        }
        return dtoList;
    }
}
