package com.zy123.affair.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zy123.affair.common.BeanConverter;
import com.zy123.affair.common.baseEntity.SwaggerPage;
import com.zy123.affair.dto.FlowNodeDto;
import com.zy123.affair.dto.FlowNodeSearchDto;
import com.zy123.affair.entity.FlowNode;
import com.zy123.affair.entity.FlowNode;
import com.zy123.affair.mapper.FlowNodeMapper;
import com.zy123.affair.service.FlowNodeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 流程模板节点 服务实现类
 * </p>
 *
 * @author lishutao
 * @since 2022-11-07
 */
@Service
public class FlowNodeServiceImpl extends ServiceImpl<FlowNodeMapper, FlowNode> implements FlowNodeService {
    @Override
    public SwaggerPage<FlowNodeDto> search(Long companyId, FlowNodeSearchDto searchDto) {
        Page<FlowNode> page = new Page<>(searchDto.getPageNum(),searchDto.getPageSize());
        QueryWrapper<FlowNode> queryWrapper = getWrapperBySearch(companyId,searchDto);
        this.page(page,queryWrapper);

        List<FlowNodeDto> dtoList = dbToDto(page.getRecords());

        SwaggerPage<FlowNodeDto> resultPage = BeanConverter.map(searchDto, SwaggerPage.class);
        resultPage.setList(dtoList);
        resultPage.setTotal((int)page.getTotal());
        return resultPage;
    }

    @Override
    public void add(Long companyId,FlowNodeDto dto) {
        FlowNode mainEntity= BeanConverter.map(dto, FlowNode.class);
     //   mainEntity.setCompanyId(companyId);
        mainEntity.insert();

    }

    @Override
    public void edit(Long companyId,FlowNodeDto dto) {
        FlowNode mainEntity= BeanConverter.map(dto, FlowNode.class);
    //    mainEntity.setCompanyId(companyId);
        mainEntity.updateById();
    }

    @Override
    public void remove(Long companyId,List<Long> idList) {
        this.removeByIds(idList);
    }

    @Override
    public FlowNodeDto get(Long companyId,Long id) {
        FlowNode entity = this.getById(id);
        return BeanConverter.map(entity,FlowNodeDto.class);
    }

    @Override
    public List<FlowNodeDto> listByIdList(Long companyId,List<Long> idList) {
        List<FlowNode> dbList = this.listByIds(idList);
        return BeanConverter.asList(dbList,FlowNodeDto.class);
    }

    @Override
    public List<FlowNodeDto> list(Long companyId,FlowNodeSearchDto searchDto) {
        QueryWrapper<FlowNode> queryWrapper = getWrapperBySearch(companyId,searchDto);
        List<FlowNode> list = this.list(queryWrapper);
        List<FlowNodeDto> dtoList = dbToDto(list);
        return dtoList;
    }




    private QueryWrapper<FlowNode> getWrapperBySearch(Long companyId,FlowNodeSearchDto searchDto) {
        FlowNode queryEntity=new FlowNode();
        QueryWrapper<FlowNode> queryWrapper = new QueryWrapper<>(queryEntity);
     //   queryWrapper.eq(FlowNode.COMPANY_ID,companyId);
    //    queryWrapper.eq(FlowNode.FORM_ID,searchDto.getFormId());

        if(CollectionUtils.isEmpty(searchDto.getOrderList())){
            queryWrapper.orderByDesc(FlowNode.CREATE_TIME);
        }

        return queryWrapper;
    }

    private List<FlowNodeDto> dbToDto(List<FlowNode> list) {
        List<FlowNodeDto> dtoList=new ArrayList<>();
        for(FlowNode tmp : list){
            FlowNodeDto tmpDto = BeanConverter.map(tmp, FlowNodeDto.class);
            dtoList.add(tmpDto);
        }
        return dtoList;
    }
}
