package com.zy123.affair.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zy123.affair.common.BeanConverter;
import com.zy123.affair.common.baseEntity.SwaggerPage;
import com.zy123.affair.dto.ViewTemplateDto;
import com.zy123.affair.dto.ViewTemplateSearchDto;
import com.zy123.affair.entity.ViewTemplate;
import com.zy123.affair.entity.ViewTemplate;
import com.zy123.affair.mapper.ViewTemplateMapper;
import com.zy123.affair.service.ViewTemplateService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 页面模板 服务实现类
 * </p>
 *
 * @author lishutao
 * @since 2022-09-27
 */
@Service
public class ViewTemplateServiceImpl extends ServiceImpl<ViewTemplateMapper, ViewTemplate> implements ViewTemplateService {
    @Override
    public SwaggerPage<ViewTemplateDto> search(ViewTemplateSearchDto searchDto) {
        Page<ViewTemplate> page = new Page<>(searchDto.getPageNum(),searchDto.getPageSize());
        QueryWrapper<ViewTemplate> queryWrapper = getWrapperBySearch(searchDto);
        this.page(page,queryWrapper);

        List<ViewTemplateDto> dtoList = dbToDto(page.getRecords());

        SwaggerPage<ViewTemplateDto> resultPage = BeanConverter.map(searchDto, SwaggerPage.class);
        resultPage.setList(dtoList);
        resultPage.setTotal((int)page.getTotal());
        return resultPage;
    }




    @Override
    public void add(ViewTemplateDto dto) {
        ViewTemplate mainEntity= BeanConverter.map(dto, ViewTemplate.class);
        mainEntity.insert();
    }

    @Override
    public void edit(ViewTemplateDto dto) {
        ViewTemplate mainEntity= BeanConverter.map(dto, ViewTemplate.class);
        mainEntity.updateById();
    }

    @Override
    public void remove(List<Long> idList) {
        this.removeByIds(idList);
    }

    @Override
    public ViewTemplateDto get(Long id) {
        ViewTemplate entity = this.getById(id);
        return BeanConverter.map(entity,ViewTemplateDto.class);
    }

    @Override
    public List<ViewTemplateDto> listByIdList(List<Long> idList) {
        List<ViewTemplate> dbList = this.listByIds(idList);
        return BeanConverter.asList(dbList,ViewTemplateDto.class);
    }

    @Override
    public List<ViewTemplateDto> list(ViewTemplateSearchDto searchDto) {
        QueryWrapper<ViewTemplate> queryWrapper = getWrapperBySearch(searchDto);
        List<ViewTemplate> list = this.list(queryWrapper);
        List<ViewTemplateDto> dtoList = dbToDto(list);
        return dtoList;
    }


    private QueryWrapper<ViewTemplate> getWrapperBySearch(ViewTemplateSearchDto searchDto) {
        ViewTemplate queryEntity=new ViewTemplate();

        QueryWrapper<ViewTemplate> queryWrapper = new QueryWrapper<>(queryEntity);

        if(CollectionUtils.isEmpty(searchDto.getOrderList())){
         //   queryWrapper.orderByDesc(ViewTemplate.CREATE_TIME);
        }

        return queryWrapper;
    }

    private List<ViewTemplateDto> dbToDto(List<ViewTemplate> list) {
        List<ViewTemplateDto> dtoList=new ArrayList<>();
        for(ViewTemplate tmp : list){
            ViewTemplateDto tmpDto = BeanConverter.map(tmp, ViewTemplateDto.class);
            dtoList.add(tmpDto);
        }
        return dtoList;
    }
}
