package com.zy123.affair.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zy123.affair.common.BeanConverter;
import com.zy123.affair.common.baseEntity.SwaggerPage;
import com.zy123.affair.dto.FlowUserDto;
import com.zy123.affair.dto.FlowUserSearchDto;
import com.zy123.affair.entity.FlowUser;
import com.zy123.affair.entity.FlowUser;
import com.zy123.affair.mapper.FlowUserMapper;
import com.zy123.affair.service.FlowUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 流程节点人员处理 服务实现类
 * </p>
 *
 * @author lishutao
 * @since 2022-11-07
 */
@Service
public class FlowUserServiceImpl extends ServiceImpl<FlowUserMapper, FlowUser> implements FlowUserService {
    @Override
    public SwaggerPage<FlowUserDto> search(Long companyId, FlowUserSearchDto searchDto) {
        Page<FlowUser> page = new Page<>(searchDto.getPageNum(),searchDto.getPageSize());
        QueryWrapper<FlowUser> queryWrapper = getWrapperBySearch(companyId,searchDto);
        this.page(page,queryWrapper);

        List<FlowUserDto> dtoList = dbToDto(page.getRecords());

        SwaggerPage<FlowUserDto> resultPage = BeanConverter.map(searchDto, SwaggerPage.class);
        resultPage.setList(dtoList);
        resultPage.setTotal((int)page.getTotal());
        return resultPage;
    }

    @Override
    public void add(Long companyId,FlowUserDto dto) {
        FlowUser mainEntity= BeanConverter.map(dto, FlowUser.class);
      //  mainEntity.setCompanyId(companyId);
        mainEntity.insert();
    }

    @Override
    public void edit(Long companyId,FlowUserDto dto) {
        FlowUser mainEntity= BeanConverter.map(dto, FlowUser.class);
     //   mainEntity.setCompanyId(companyId);
        mainEntity.updateById();
    }

    @Override
    public void remove(Long companyId,List<Long> idList) {
        this.removeByIds(idList);
    }

    @Override
    public FlowUserDto get(Long companyId,Long id) {
        FlowUser entity = this.getById(id);
        return BeanConverter.map(entity,FlowUserDto.class);
    }

    @Override
    public List<FlowUserDto> listByIdList(Long companyId,List<Long> idList) {
        List<FlowUser> dbList = this.listByIds(idList);
        return BeanConverter.asList(dbList,FlowUserDto.class);
    }

    @Override
    public List<FlowUserDto> list(Long companyId,FlowUserSearchDto searchDto) {
        QueryWrapper<FlowUser> queryWrapper = getWrapperBySearch(companyId,searchDto);
        List<FlowUser> list = this.list(queryWrapper);
        List<FlowUserDto> dtoList = dbToDto(list);
        return dtoList;
    }

    
    private QueryWrapper<FlowUser> getWrapperBySearch(Long companyId,FlowUserSearchDto searchDto) {
        FlowUser queryEntity=new FlowUser();
        QueryWrapper<FlowUser> queryWrapper = new QueryWrapper<>(queryEntity);
    //    queryWrapper.eq(FlowUser.COMPANY_ID,companyId);
     //   queryWrapper.eq(FlowUser.FORM_ID,searchDto.getFormId());

        if(CollectionUtils.isEmpty(searchDto.getOrderList())){
            queryWrapper.orderByDesc(FlowUser.CREATE_TIME);
        }

        return queryWrapper;
    }

    private List<FlowUserDto> dbToDto(List<FlowUser> list) {
        List<FlowUserDto> dtoList=new ArrayList<>();
        for(FlowUser tmp : list){
            FlowUserDto tmpDto = BeanConverter.map(tmp, FlowUserDto.class);
            dtoList.add(tmpDto);
        }
        return dtoList;
    }
}
