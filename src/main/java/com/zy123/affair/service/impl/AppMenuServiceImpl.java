package com.zy123.affair.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zy123.affair.common.BeanConverter;
import com.zy123.affair.common.ListTreeUtil;
import com.zy123.affair.dto.AppDto;
import com.zy123.affair.dto.AppMenuDto;
import com.zy123.affair.dto.TreeNodeDto;
import com.zy123.affair.entity.App;
import com.zy123.affair.entity.AppMenu;
import com.zy123.affair.mapper.AppMenuMapper;
import com.zy123.affair.service.AppMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * <p>
 * 应用页面导航菜单 服务实现类
 * </p>
 *
 * @author lishutao
 * @since 2022-09-27
 */
@Service
public class AppMenuServiceImpl extends ServiceImpl<AppMenuMapper, AppMenu> implements AppMenuService {

    /**
     * 获取完整树
     *
     * @param companyId
     */
    @Override
    public List<TreeNodeDto> tree(Long appId) {
        QueryWrapper queryWrapper=new QueryWrapper();
        queryWrapper.eq(AppMenu.APP_ID,appId);
        List<AppMenu> list = this.list(queryWrapper);

        List<TreeNodeDto> dtoList = BeanConverter.asList(list, TreeNodeDto.class);
        List<TreeNodeDto> treeList = ListTreeUtil.listToTree(dtoList, -1L);
        return treeList;
    }

    @Override
    public void addNode(Long appId,TreeNodeDto node) {
        AppMenu entity=BeanConverter.map(node,AppMenu.class);
        entity.setAppId(appId);
        this.save(entity);
    }

    @Override
    public void editNode(TreeNodeDto node) {
        AppMenu entity=BeanConverter.map(node,AppMenu.class);
        this.updateById(entity);
    }

    /**
     * 删除
     *
     * @param idList
     */
    @Override
    public void removeNode(Long appId,List<Long> idList) {
        QueryWrapper queryWrapper=new QueryWrapper();
        queryWrapper.eq(AppMenu.APP_ID,appId);
        List<AppMenu> dbList = this.list(queryWrapper);

        List<AppMenu>  subList=new ArrayList<>();
        for(Long tmpId:idList){
            List<AppMenu> tmpSubList = ListTreeUtil.getFullNode(dbList, tmpId);
            subList.addAll(tmpSubList);
        }
        List<Long> tmpSubIdList = subList.stream().map(e -> e.getId()).collect(Collectors.toList());
        idList.addAll(tmpSubIdList);

        this.removeByIds(idList);
    }

    /**
     * 获取全路径
     *
     * @param id
     * @return
     */
    @Override
    public List<TreeNodeDto> getFullPath(Long appId,Long id) {
        List<AppMenu> list = this.list();
        QueryWrapper queryWrapper=new QueryWrapper();
        queryWrapper.eq(AppMenu.APP_ID,appId);
        Map<Long, AppMenu> map = list.stream().collect(Collectors.toMap(e -> e.getId(), Function.identity(), (o1, o2) -> o2));

        List<AppMenu> pathList=new ArrayList<>();
        Long tmpId= id;
        Integer maxLimit=10;
        for(int i=0;i<maxLimit;i++){
            AppMenu tmp = map.get(tmpId);
            pathList.add(0,tmp);
            if(tmp.getParentId().equals(-1L)){
                break;
            }
            tmpId = tmp.getParentId();
        }

        List<TreeNodeDto> dtoList = BeanConverter.asList(pathList, TreeNodeDto.class);
        return dtoList;
    }

    /**
     * 获取所有子节点
     *
     * @param id
     */
    @Override
    public List<TreeNodeDto> listSubNode(Long appId,Long id) {

        QueryWrapper queryWrapper=new QueryWrapper();
        queryWrapper.eq(AppMenu.APP_ID,appId);
        List<AppMenu> list = this.list();
        List<AppMenu> fullNodeList = ListTreeUtil.getFullNode(list, id);
        List<TreeNodeDto> dtoList = BeanConverter.asList(fullNodeList, TreeNodeDto.class);
        return dtoList;
    }

    @Override
    public void edit(AppMenuDto dto) {
        AppMenu mainEntity= BeanConverter.map(dto, AppMenu.class);
        mainEntity.updateById();
    }

    @Override
    public AppMenuDto get(Long id) {
        AppMenu entity = this.getById(id);
        return BeanConverter.map(entity,AppMenuDto.class);
    }



}
