package com.zy123.affair.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zy123.affair.common.BeanConverter;
import com.zy123.affair.common.baseEntity.SwaggerPage;
import com.zy123.affair.dto.FlowDataDto;
import com.zy123.affair.dto.FlowDataSearchDto;
import com.zy123.affair.entity.FlowData;
import com.zy123.affair.entity.FlowData;
import com.zy123.affair.mapper.FlowDataMapper;
import com.zy123.affair.service.FlowDataService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 流程节点数据 服务实现类
 * </p>
 *
 * @author lishutao
 * @since 2022-11-07
 */
@Service
public class FlowDataServiceImpl extends ServiceImpl<FlowDataMapper, FlowData> implements FlowDataService {
    @Override
    public SwaggerPage<FlowDataDto> search(Long companyId, FlowDataSearchDto searchDto) {
        Page<FlowData> page = new Page<>(searchDto.getPageNum(),searchDto.getPageSize());
        QueryWrapper<FlowData> queryWrapper = getWrapperBySearch(companyId,searchDto);
        this.page(page,queryWrapper);

        List<FlowDataDto> dtoList = dbToDto(page.getRecords());

        SwaggerPage<FlowDataDto> resultPage = BeanConverter.map(searchDto, SwaggerPage.class);
        resultPage.setList(dtoList);
        resultPage.setTotal((int)page.getTotal());
        return resultPage;
    }

    @Override
    public void add(Long companyId,FlowDataDto dto) {
        FlowData mainEntity= BeanConverter.map(dto, FlowData.class);
     //   mainEntity.setCompanyId(companyId);
        mainEntity.insert();
    }

    @Override
    public void edit(Long companyId,FlowDataDto dto) {
        FlowData mainEntity= BeanConverter.map(dto, FlowData.class);
     //   mainEntity.setCompanyId(companyId);
        mainEntity.updateById();
    }

    @Override
    public void remove(Long companyId,List<Long> idList) {
        this.removeByIds(idList);
    }

    @Override
    public FlowDataDto get(Long companyId,Long id) {
        FlowData entity = this.getById(id);
        return BeanConverter.map(entity,FlowDataDto.class);
    }

    @Override
    public List<FlowDataDto> listByIdList(Long companyId,List<Long> idList) {
        List<FlowData> dbList = this.listByIds(idList);
        return BeanConverter.asList(dbList,FlowDataDto.class);
    }

    @Override
    public List<FlowDataDto> list(Long companyId,FlowDataSearchDto searchDto) {
        QueryWrapper<FlowData> queryWrapper = getWrapperBySearch(companyId,searchDto);
        List<FlowData> list = this.list(queryWrapper);
        List<FlowDataDto> dtoList = dbToDto(list);
        return dtoList;
    }



    private QueryWrapper<FlowData> getWrapperBySearch(Long companyId,FlowDataSearchDto searchDto) {
        FlowData queryEntity=new FlowData();
        QueryWrapper<FlowData> queryWrapper = new QueryWrapper<>(queryEntity);
     //   queryWrapper.eq(FlowData.COMPANY_ID,companyId);
     //   queryWrapper.eq(FlowData.FORM_ID,searchDto.getFormId());

        if(CollectionUtils.isEmpty(searchDto.getOrderList())){
            queryWrapper.orderByDesc(FlowData.CREATE_TIME);
        }

        return queryWrapper;
    }

    private List<FlowDataDto> dbToDto(List<FlowData> list) {
        List<FlowDataDto> dtoList=new ArrayList<>();
        for(FlowData tmp : list){
            FlowDataDto tmpDto = BeanConverter.map(tmp, FlowDataDto.class);
            dtoList.add(tmpDto);
        }
        return dtoList;
    }
}
