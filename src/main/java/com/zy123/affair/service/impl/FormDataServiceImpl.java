package com.zy123.affair.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zy123.affair.baseenum.formAttr.FormAttrTypeEnum;
import com.zy123.affair.baseenum.formAttr.FormAttrValueCountTypeEnum;
import com.zy123.affair.common.BeanConverter;
import com.zy123.affair.common.baseEntity.SwaggerPage;
import com.zy123.affair.common.jackson.JsonUtils;
import com.zy123.affair.dto.FormDataSearchDto;
import com.zy123.affair.entity.FormAttr;
import com.zy123.affair.entity.FormData;
import com.zy123.affair.entity.FormData;
import com.zy123.affair.entity.formAttr.FormAttrValueScope;
import com.zy123.affair.mapper.FormDataMapper;
import com.zy123.affair.service.FormAttrService;
import com.zy123.affair.service.FormDataService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 表单属性 服务实现类
 * </p>
 *
 * @author lishutao
 * @since 2022-09-27
 */
@Service
public class FormDataServiceImpl extends ServiceImpl<FormDataMapper, FormData> implements FormDataService {
    @Autowired
    private FormAttrService formAttrService;

    @Override
    public SwaggerPage<Map<String,Object>> advSearch(Long formId, Map<String,Object> searchMap) {
        return null;
    }

    @Override
    public SwaggerPage<Map<String,Object>> search(Long formId,FormDataSearchDto searchDto) {
        Page<FormData> page = new Page<>(searchDto.getPageNum(),searchDto.getPageSize());

        QueryWrapper<FormData> queryWrapper = getWrapperBySearch(formId,searchDto);
        this.page(page,queryWrapper);

        List<Map<String,Object>> dtoList = dbToDto(formId,page.getRecords());

        SwaggerPage<Map<String,Object>> resultPage = BeanConverter.map(searchDto, SwaggerPage.class);
        resultPage.setList(dtoList);
        resultPage.setTotal((int)page.getTotal());
        return resultPage;
    }




    @Override
    public void add(Long formId,Map<String,Object> entity) {
        FormData mainEntity= new FormData();
        mainEntity.setFormId(formId);
        mainEntity.setDataJson(JsonUtils.toJson(entity));

        //TODO 查询需要的冗余字段，获取页面数据冗余

        mainEntity.insert();
    }

    @Override
    public void edit(Long formId,Map<String,Object> entity) {
        FormData mainEntity= new FormData();
        String id = (String) entity.get("id");
        mainEntity.setId(Long.valueOf(id));

        //TODO 查询需要的冗余字段，获取页面数据冗余

        mainEntity.updateById();
    }

    @Override
    public void remove(Long formId,List<Long> idList) {
        this.removeByIds(idList);
    }

    @Override
    public Map<String,Object> get(Long formId,Long id) {
        FormData entity = this.getById(id);
        return dbToDto(formId,entity);
    }

    @Override
    public List<Map<String,Object>> listByIdList(Long formId,List<Long> idList) {
        List<FormData> dbList = this.listByIds(idList);
        return dbToDto(formId,dbList);
    }

    @Override
    public List<Map<String,Object>> list(Long formId,FormDataSearchDto searchDto) {
        QueryWrapper<FormData> queryWrapper = getWrapperBySearch(formId,searchDto);
        List<FormData> dbList = this.list(queryWrapper);
        return dbToDto(formId,dbList);
    }


    private QueryWrapper<FormData> getWrapperBySearch(Long formId,FormDataSearchDto searchDto) {
        QueryWrapper<FormData> queryWrapper = new QueryWrapper<>();
        //TODO 查找字段转冗余字段

        if(CollectionUtils.isEmpty(searchDto.getOrderList())){
            queryWrapper.orderByDesc(FormData.CREATE_TIME);
        }

        return queryWrapper;
    }

    private List<Map<String,Object>> dbToDto(Long formId,List<FormData> list) {
        //获取返回值信息
        List<Map<String,Object>> mapList=new ArrayList<>(list.size());
        for(FormData tmpData:list){
            String dataJson = tmpData.getDataJson();
            Map<String,Object> map = JsonUtils.toObject(dataJson, Map.class);
        }

        //设置关联数据
        setShowName(formId, mapList);

        return mapList;
    }

    private Map<String,Object> dbToDto(Long formId,FormData dbEntity) {
        String dataJson = dbEntity.getDataJson();
        Map<String,Object> map = JsonUtils.toObject(dataJson, Map.class);
        List<Map<String, Object>> mapList = new ArrayList<>(Arrays.asList(map));
        //设置关联数据
        setShowName(formId, mapList);
        return map;
    }

    private void setShowName(Long formId, List<Map<String, Object>> mapList) {
        List<FormAttr> formAttrList = formAttrService.listByFormId(formId);

        for (FormAttr tmp : formAttrList) {
            //表单数据
            if (FormAttrTypeEnum.FORM.getValue().equals(tmp.getType())) {
                setShowNameByForm(mapList, tmp);
                continue;
            }

            if (FormAttrTypeEnum.SELECT.getValue().equals(tmp.getType())) {
                setShowNameBySelect(mapList, tmp);
            }
        }
    }

    private void setShowNameBySelect(List<Map<String, Object>> list, FormAttr tmp){
        FormAttrValueScope valueScope = JsonUtils.toObject(tmp.getValueScope(), FormAttrValueScope.class);
        Map<Integer, String> nameMap = valueScope.getSelectList().stream()
                .collect(Collectors.toMap(e -> e.getKey(), e -> e.getName(), (o1, o2) -> o1));
        for(Map<String,Object> map:list){
            Object value = map.get(tmp.getPyName());

            if(value == null){
                continue;
            }

            String showName="";
            if (FormAttrValueCountTypeEnum.SINGLE.getValue().equals(tmp.getValueCountType())) {
                showName = nameMap.get((Integer) value);
            } else {
                //添加Id
                boolean isFirst=true;
                for(String temVal: (List<String>) value){
                    if(isFirst){
                        showName = nameMap.get((Integer) value);;
                    }else {
                        showName += "," + nameMap.get((Integer) value);;

                    };
                }
            }
            map.put(tmp.getPyName()+"@ShowName",showName);
        }
    }

    private void setShowNameByForm(List<Map<String, Object>> list, FormAttr tmp) {
        //获取关联数据Id
        List<Long> idList= new ArrayList<>();
        for(Map<String,Object> map:list){
            Object value = map.get(tmp.getPyName());
            if(value == null){
                continue;
            }

            if (FormAttrValueCountTypeEnum.SINGLE.getValue().equals(tmp.getValueCountType())) {
                idList.add(Long.valueOf((String) value));
            } else {
                //添加Id
                for(String temVal: (List<String>) value){
                    idList.add(Long.valueOf((String) value));
                }
            }
        }

        if(idList.size() == 0){
           return;
        }

        //设置LinkName的值
        List<FormData> formDataList = this.listByIds(idList);
        Map<Long, Map<String,Object>> formDataMap = formDataList.stream()
                .collect(Collectors.toMap(e -> e.getId(), e -> JsonUtils.toObject(e.getDataJson(), Map.class), (o1, o2) -> o1));

        for(Map<String,Object> map:list){
            Object value = map.get(tmp.getPyName());

            if(value == null){
                continue;
            }

            String showName="";
            if (FormAttrValueCountTypeEnum.SINGLE.getValue().equals(tmp.getValueCountType())) {
                Map<String, Object> linkMap = formDataMap.get(Long.valueOf((String) value));
                showName = (String) linkMap.get(tmp.getShowAttr());
            } else {
                //添加Id
                boolean isFirst=true;
                for(String temVal: (List<String>) value){
                    Map<String, Object> linkMap = formDataMap.get(Long.valueOf((String) value));
                    if(isFirst){
                        showName = (String) linkMap.get(tmp.getShowAttr());
                    }else {
                        showName += "," + (String) linkMap.get(tmp.getShowAttr());

                    };
                }
            }
            map.put(tmp.getPyName()+"@ShowName",showName);
        }
    }
}
