package com.zy123.affair.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zy123.affair.common.baseEntity.SwaggerPage;
import com.zy123.affair.dto.FormDataSearchDto;
import com.zy123.affair.entity.FormData;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 表单属性 服务类
 * </p>
 *
 * @author lishutao
 * @since 2022-09-27
 */
public interface FormDataService extends IService<FormData> {
    SwaggerPage<Map<String,Object>> advSearch(Long formId, Map<String,Object> searchMap);

    SwaggerPage<Map<String,Object>> search(Long formId,FormDataSearchDto searchDto);

    void add(Long formId,Map<String,Object> dto);

    void edit(Long formId,Map<String,Object> dto);

    void remove(Long formId,List<Long> idList);

    Map<String,Object> get(Long formId,Long id);

    List<Map<String,Object>> listByIdList(Long formId,List<Long> idList);

    List<Map<String,Object>> list(Long formId,FormDataSearchDto searchDto);
}
