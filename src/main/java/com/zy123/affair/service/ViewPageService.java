package com.zy123.affair.service;

import com.zy123.affair.common.baseEntity.SwaggerPage;
import com.zy123.affair.dto.ViewPageDto;
import com.zy123.affair.dto.ViewPageSearchDto;
import com.zy123.affair.entity.ViewPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zy123.affair.entity.x6.SceneViewCell;
import com.zy123.affair.entity.x6.ViewPageNode;

import java.util.List;

/**
 * <p>
 * 模板页面 服务类
 * </p>
 *
 * @author lishutao
 * @since 2022-09-27
 */
public interface ViewPageService extends IService<ViewPage> {


    void edit(ViewPageDto dto);

    ViewPageDto get(Long id);

    List<ViewPageDto> listByAppMenuId(Long appMenuId);

    void resetByFlow(List<SceneViewCell<ViewPageNode>> nodeList,Long appMenuId);

}
