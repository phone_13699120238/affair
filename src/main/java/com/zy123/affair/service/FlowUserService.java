package com.zy123.affair.service;

import com.zy123.affair.common.baseEntity.SwaggerPage;
import com.zy123.affair.dto.FlowUserDto;
import com.zy123.affair.dto.FlowUserSearchDto;
import com.zy123.affair.entity.FlowUser;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 流程节点人员处理 服务类
 * </p>
 *
 * @author lishutao
 * @since 2022-11-07
 */
public interface FlowUserService extends IService<FlowUser> {
    SwaggerPage<FlowUserDto> search(Long companyId, FlowUserSearchDto searchDto);

    void add(Long companyId,FlowUserDto dto);

    void edit(Long companyId,FlowUserDto dto);

    void remove(Long companyId, List<Long> idList);

    FlowUserDto get(Long companyId,Long id);

    List<FlowUserDto> listByIdList(Long companyId,List<Long> idList);

    List<FlowUserDto> list(Long companyId,FlowUserSearchDto searchDto);
}
