package com.zy123.affair.service;

import com.zy123.affair.common.baseEntity.SwaggerPage;
import com.zy123.affair.dto.ViewTemplateDto;
import com.zy123.affair.dto.ViewTemplateSearchDto;
import com.zy123.affair.entity.ViewTemplate;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 页面模板 服务类
 * </p>
 *
 * @author lishutao
 * @since 2022-09-27
 */
public interface ViewTemplateService extends IService<ViewTemplate> {
    SwaggerPage<ViewTemplateDto> search(ViewTemplateSearchDto searchDto);

    void add(ViewTemplateDto dto);

    void edit(ViewTemplateDto dto);

    void remove(List<Long> idList);

    ViewTemplateDto get(Long id);

    List<ViewTemplateDto> listByIdList(List<Long> idList);

    List<ViewTemplateDto> list(ViewTemplateSearchDto searchDto);
}
