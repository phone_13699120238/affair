package com.zy123.affair.common;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Random;

/**
 * @author wugaoping on 2017-12-06
 **/
public class PasswordUtil {
    public final static String PASSWORD_SPLITOR = "\007";
    public final static int PASSWORD_LENGTH = 32;
    /**
     * 生成加密密码
     * @param accountName 账号名称
     * @param plainPassword 明文密码
     * @return
     */
    public static String getPassword(String accountName, String plainPassword) {
        StringBuilder sb = new StringBuilder();
        sb.append(plainPassword).append("acs").append(accountName).append("salt");
        return DigestUtils.md5Hex(sb.toString());
    }

    public static String getStorePassword(String password) {
        String salt = DigestUtils.md5Hex(generateString(PASSWORD_LENGTH));
        return getStorePassword(salt, password);
    }

    /**
     * 返回一个定长的随机字符串(只包含大小写字母、数字)
     *
     * @param length 随机字符串长度
     * @return 随机字符串
     */
    public static String generateString(int length) {
        String allChar = "012356789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        StringBuffer sb = new StringBuffer();
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            sb.append(allChar.charAt(random.nextInt(allChar.length())));
        }
        return sb.toString();
    }


    public static String getStorePassword(String salt, String password) {
        String saltPassword = DigestUtils.md5Hex(String.format("%s%s%s", salt, PASSWORD_SPLITOR, password));
        String storePassword = String.format("%s%s%s", salt, PASSWORD_SPLITOR, saltPassword);
        return storePassword;
    }

    public static boolean isSamePassword(String password, String storePassword) {
        if (StringUtils.isEmpty(storePassword)) {
            return false;
        }

        String[] saltPassword = storePassword.split(PASSWORD_SPLITOR);
        if (saltPassword.length != 2) {
            return false;
        }

        String sp = getStorePassword(saltPassword[0], password);
        if (storePassword.equals(sp)) {
            return true;
        }

        return false;
    }

   public static void main(String[] args) {
        String storePassword1 = getStorePassword("e95745fdfb6e8f9d750e709c6a6065d5");
        System.out.println(storePassword1);
    }
}
