package com.zy123.affair.common.exception;

/**
 * token异常
 * @author wugaoping on 2017-10-20
 */
public final class TokenException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String errorCode = TpErrorCodeGeneral.RESULT_CODE_ERR_TOKEN;

	public TokenException(String message) {
		super(message);
	}
}
