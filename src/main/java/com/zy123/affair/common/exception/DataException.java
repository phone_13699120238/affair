package com.zy123.affair.common.exception;

/**
 * @author wugaoping on 2017-10-19
 */
public final class DataException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String className;
	public String errorCode = TpErrorCodeGeneral.RESULT_CODE_ERR_DATA;

	public DataException(String message) {
		super(message);
	}

}
