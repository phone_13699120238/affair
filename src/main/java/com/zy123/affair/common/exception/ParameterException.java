package com.zy123.affair.common.exception;

/**
 * 参数异常
 *
 * @author guoyongshi
 */
public final class ParameterException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    public String errorCode = TpErrorCodeGeneral.RESULT_CODE_ERR_PARAM;
    public String errorParamer = null;

    public ParameterException(String message, String errorParamer) {
        super(message);
        this.errorParamer = errorParamer;
    }

    public ParameterException(String message) {
        super(message);
    }
}
