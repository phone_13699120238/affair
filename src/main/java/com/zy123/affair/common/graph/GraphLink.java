package com.zy123.affair.common.graph;

import lombok.Data;

/**
 * @author list
 */
@Data
public class GraphLink {
    private String sourseNodeKey;
    private String targetNodeKey;
    public GraphLink(String sourseNodeKey,String targetNodeKey){
        this.sourseNodeKey = sourseNodeKey;
        this.targetNodeKey = targetNodeKey;
    }
}
