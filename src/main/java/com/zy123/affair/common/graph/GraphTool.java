package com.zy123.affair.common.graph;

import com.zy123.affair.common.BeanConverter;
import org.apache.commons.lang3.RandomUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author list
 */
public class GraphTool {
    private List<GraphLink> graphLinkList;
    private List<String> nodeList;
    private String startNodeKey;
    private String endNodeKey;

    // 定义一个图
    private Integer[][] graph;
    // visit数组，用于在dfs中记录访问过的顶点信息。
    private int[] visit;
    //存储每条可能的路径
    private List path = new ArrayList<>();
    // 用于存储所有路径的集合
    private List<List<String>> allPath = new ArrayList<>();
    //起点和终点
    private int start;
    private int end;

    public GraphTool(List<GraphLink> graphLinkList,List<String> nodeList,String startNodeKey,String endNodeKey){
         this.graphLinkList = graphLinkList;
         this.nodeList = nodeList;
         this.startNodeKey=startNodeKey;
         this.endNodeKey =endNodeKey;
    }
    private void toGraph(){
        graph =new Integer[nodeList.size()][nodeList.size()];
        for(GraphLink tmp:graphLinkList){
            int i = nodeList.indexOf(tmp.getSourseNodeKey());
            int j = nodeList.indexOf(tmp.getTargetNodeKey());
            graph[i][j]=1;
        }

        this.start = nodeList.indexOf(startNodeKey);
        this.end = nodeList.indexOf(endNodeKey);
        visit = new int[nodeList.size()];
    }

    public List<List<String>> allPath(){
        toGraph();
        dfs(start);
        return allPath;
    }

    public List<String> randomPath(){
        allPath();
        int i = 0;
        if (allPath.size() > 1) {
             i = RandomUtils.nextInt(0, allPath.size());
        }
        return allPath.get(i);
    }

    private void dfs(int u){
        visit[u] = 1;
        path.add(nodeList.get(u));
        if(u == end){
            List<String> newPath = BeanConverter.asList(path, String.class);
            allPath.add(newPath);
        }else{
            for (int i = 0; i < nodeList.size(); i++) {
                if(visit[i]==0&&i!=u && graph[u] !=null && graph[u][i] != null && graph[u][i]==1){
                    dfs(i);
                }
            }
        }
        path.remove(path.size()-1);
        visit[u] = 0;
    }

}
