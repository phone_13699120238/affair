package com.zy123.affair.common.baseEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author list
 */
@ApiModel(value="分页",description="分页通用对象")
@Data
public class SwaggerPage<T> {
    @ApiModelProperty(value="排序规则集合",name="orderList")
    protected List<SwaggerOrder> orderList;

    @ApiModelProperty(value="页码",name="pageNum",example="1")
    protected Integer pageNum;

    @ApiModelProperty(value="每页条码数量",name="pageSize",example="10")
    protected Integer pageSize;

    @ApiModelProperty(value="总条码数",name="total")
    protected Integer total;

    @ApiModelProperty(value="数据集合",name="list")
    protected List<T> list;
}

