package com.zy123.affair.common.baseEntity;

import com.zy123.affair.common.exception.TpErrorCodeGeneral;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@ApiModel(value="通用返回对象",description="通用返回对象")
@Data
public class ResultObj<T> implements Serializable{

	@ApiModelProperty(value="返回状态 " +
			"10000：正常；20000：参数错误；30000：系统错误；" +
			"40000：通知用户错误；50000：token 错误；60000：数据 错误; 70000:权限 错误; 80000: 接口访问过快 错误; ",name="status")
	private String code = TpErrorCodeGeneral.RESULT_CODE_OK;

	@ApiModelProperty(value="数据载体，可以是任何各种类型的数据",name="data")
	private T data;
	@ApiModelProperty(value="消息提示信息，正确时ok，错误时提示错误信息",name="msg")
	private String msg ="ok";

	@ApiModelProperty(value="接口请求返回时间")
	private Long reqTime;

	public ResultObj(){
		reqTime=System.currentTimeMillis();
	}

	public ResultObj(String code,String msg){
          this.code = code;
          this.msg = msg;
		  reqTime=System.currentTimeMillis();
	}

	public ResultObj(T data){
        this.data = data;
		reqTime=System.currentTimeMillis();
	}

}
