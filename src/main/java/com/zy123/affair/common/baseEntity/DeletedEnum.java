package com.zy123.affair.common.baseEntity;

/**
 * @author list
 */

public enum  DeletedEnum {
    NORMAL(1,"正常"),
    DELETED(0,"已删除");

    private Integer value;
    private String name;

    DeletedEnum(Integer value,String name){
        this.value =value;
        this.name = name;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}
