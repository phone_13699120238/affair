package com.zy123.affair.common.baseEntity;

import lombok.Data;

/**
 * @author list
 */
@Data
public class BaseDic {
    private Integer key;
    private String value;
}
