package com.zy123.affair.common.baseEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author list
 */
@Data
@ApiModel(value="排序对象",description="分页通用对象")
public class SwaggerOrder {
    @ApiModelProperty(value="排序字段",name="name",example="createTime")
    protected String name;

    @ApiModelProperty(value="排序规则 正序: asc 倒序: desc",name="name")
    protected String sort;
}
