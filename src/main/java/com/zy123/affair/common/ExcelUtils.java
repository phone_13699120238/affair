package com.zy123.affair.common;

import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

public class ExcelUtils {
    /**
     * 导出设置输出
     * @param resp
     */
    public static void exportSetResp(HttpServletResponse resp,String fileName){
        try {
            resp.setContentType("octets/stream");
            resp.addHeader("Content-Disposition", "attachment;filename="
                    + URLEncoder.encode(fileName, "utf-8"));
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public static void main(String[] args) throws UnsupportedEncodingException {
        String str="我的明天";
        String encode = URLEncoder.encode(str, "utf-8");
        System.out.println(encode);
        String decode = URLDecoder.decode(encode, "utf-8");
        System.out.println(decode);
    }
}
