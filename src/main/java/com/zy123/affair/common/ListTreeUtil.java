package com.zy123.affair.common;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ListTreeUtil {

    /**
     *  list 转为 tree 保存原始顺序
     * @param list
     * @param rootId
     * @param <K>
     * @param <T>
     * @return
     */
    public static <K,T> List<T> listToTree(List<T> list,K rootId){
        String idName="id";
        String pidName="parentId";
        String childName="list";
        List<T>  repeatList= ListMapUtil.repeat(list,idName);
        Map<K, List<T>> pidMap = ListMapUtil.listToMapList(repeatList, pidName);
        List<T> treeList = pidMap.get(rootId);
        //查找不到第一层，默认返回空集合
        if(treeList == null){
            return new ArrayList<>(0);
        }
        fillTree(pidMap,treeList,idName,childName);
        return treeList;
    }

    /**
     *  获取所有子节点
     * @param list
     * @param rootId
     * @param <K>
     * @param <T>
     * @return
     */
    public static <K,T> List<T>  getFullNode(List<T> list,K rootId){
        String idName="id";
        String pidName="parentId";
        List<T> allList=new ArrayList<>();
        Map<K, List<T>> pidMap = ListMapUtil.listToMapList(list, pidName);
        getSubNode(pidMap,allList,rootId,idName);
        return  allList;
    }

    private static <K,T> void  fillTree(Map<K, List<T>> map, List<T> parentList,String idName,String childName){
        for(T temp:parentList){
            K id= (K) ReflectionUtil.getFieldValue(temp, idName);
            List<T> childList = map.get(id);
            if(childList == null || childList.size() == 0){
                continue;
            }
            ReflectionUtil.setFieldValue(temp,childName,childList);
            fillTree(map,childList,idName,childName);
        }
    }

    private static <K,T> void getSubNode(Map<K, List<T>> map,List<T> allList,K parentNodeId,String idName){
            List<T> childList = map.get(parentNodeId);
            if(childList == null || childList.size() == 0){
               return;
            }
            allList.addAll(childList);
            for(T childNode:childList){
                K id= (K) ReflectionUtil.getFieldValue(childNode, idName);
                getSubNode(map,allList,id,idName);
            }
    }


}
