package com.zy123.affair.cache.common;

import com.fasterxml.jackson.core.type.TypeReference;

import com.zy123.affair.common.exception.DataException;
import com.zy123.affair.common.jackson.JsonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.StringRedisConnection;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.TimeUnit;

@Component
public class CommonStringCache {
    @Autowired
    protected RedisTemplate<String, String> redisTemplate;

    @Value("${redis.data.time:518400}")
    private Long cacheTime;
    @Value("${redis.data.random.time:600}")
    private Integer randomTime;
    @Value("${redis.data.prefix:queresource}")
    private String prefix;

    protected void baseSet(Object id, Object value) {
        String key = key(id);
        if (value instanceof String) {
            this.redisTemplate.opsForValue().set(key, (String) value);
            return;
        }
        this.redisTemplate.opsForValue().set(key, JsonUtils.toJson(value));
    }

    protected void baseSetAndTime(Object id, Object value) {
        String key = key(id);
        if (value instanceof String) {
            this.redisTemplate.opsForValue().set(key, (String) value, cacheTime, TimeUnit.SECONDS);
            return;
        }
        this.redisTemplate.opsForValue().set(key, JsonUtils.toJson(value), cacheTime, TimeUnit.SECONDS);
    }

    protected <X> void baseSet(Map<X, ?> map) {
        if (map.size() == 0) {
            return;
        }
        Map<String, String> strMap = new HashMap(map.size());
        for (Map.Entry<X, ?> entry : map.entrySet()) {
            X id = entry.getKey();
            Object value = entry.getValue();

            String key = this.key(id);
            if (value instanceof String) {
                strMap.put(key, (String) value);
                continue;
            }
            strMap.put(key, JsonUtils.toJson(value));
        }

        this.redisTemplate.opsForValue().multiSet(strMap);
    }

    protected <X> void baseSetAndTime(Map<X, ?> map) {
        if (map.size() == 0) {
            return;
        }
        this.redisTemplate.executePipelined(new RedisCallback() {
            @Override
            public Object doInRedis(RedisConnection redisConnection) throws DataAccessException {
                StringRedisConnection conn = (StringRedisConnection) redisConnection;
                //有效期进行加减防止雪崩
                Random rm = new Random();

                for (Map.Entry<X, ?> entry : map.entrySet()) {
                    int randomNo = rm.nextInt(randomTime);
                    X id = entry.getKey();
                    Object value = entry.getValue();

                    String key = key(id);
                    if (value instanceof String) {
                        conn.setEx(key, cacheTime - randomNo, (String) value);
                        continue;
                    }
                    conn.setEx(key, cacheTime - randomNo, JsonUtils.toJson(value));
                }
                return null;
            }
        });
    }

    /**
     * @param id
     * @return
     */
    protected <X> CacheOptResult<String, X> baseGet(X id) {
        String key = key(id);
        String strValue = redisTemplate.opsForValue().get(key);
        CacheOptResult<String, X> rlt = new CacheOptResult<>();
        if (strValue == null) {
            rlt.setNoCacheId(id);
            return rlt;
        }
        //防止击穿的默认值测试
        if (strValue.equals("")) {
            return rlt;
        }
        rlt.setCacheData(strValue);
        return rlt;
    }


    /**
     * @param id
     * @param cls
     * @param <T>
     * @return
     */
    protected <T, X> CacheOptResult<T, X> baseGet(X id, Class<T> cls) {
        String key = key(id);
        String strValue = redisTemplate.opsForValue().get(key);

        CacheOptResult<T, X> rlt = new CacheOptResult<>();
        if (strValue == null) {
            rlt.setNoCacheId(id);
            return rlt;
        }
        //防止击穿的默认值测试
        if (strValue.equals("")) {
            return rlt;
        }
        rlt.setCacheData(JsonUtils.toObject(strValue, cls));
        return rlt;
    }

    /**
     * @param id
     * @param typeRef
     * @param <T>
     * @return
     */
    protected <T, X> CacheOptResult<T, X> baseGet(X id, TypeReference<T> typeRef) {
        String key = key(id);
        String strValue = redisTemplate.opsForValue().get(key);

        CacheOptResult<T, X> rlt = new CacheOptResult<>();
        if (strValue == null) {
            rlt.setNoCacheId(id);
            return rlt;
        }
        //防止击穿的默认值测试
        if (strValue.equals("")) {
            return rlt;
        }
        //转换为对象
        T entity = JsonUtils.complexToObject(strValue, typeRef);
        rlt.setCacheData(entity);
        return rlt;

    }

    /**
     * @param idList
     * @return object[0]  缓存中的对象  object[1] 缓存中不存在的对象Id
     */
    protected <X> CacheOptResult<List<String>, List<X>> baseGet(List<X> idList) {
        //构造返回值
        List<X> noCacheIdList = new ArrayList<>();
        List<String> entityList = new ArrayList<>();
        CacheOptResult<List<String>, List<X>> rlt = new CacheOptResult<>();
        rlt.setNoCacheId(noCacheIdList);
        rlt.setCacheData(entityList);

        //转换key
        List<String> keyList = keyList(idList);
        //获取值
        List<String> redisList = redisTemplate.opsForValue().multiGet(keyList);
        for (int i = 0; i < idList.size(); i++) {
            String temp = redisList.get(i);
            if (temp == null) {
                noCacheIdList.add(idList.get(i));
                continue;
            }
            //防止击穿的默认值测试
            if (temp.equals("")) {
                continue;
            }
            //转换为对象
            entityList.add(temp);
        }
        return rlt;
    }

    /**
     * @param idList
     * @return object[0]  缓存中的对象  object[1] 缓存中不存在的对象Id
     */
    protected <T, X> CacheOptResult<List<T>, List<X>> baseGet(List<X> idList, Class<T> cls) {
        //构造返回值
        List<X> noCacheIdList = new ArrayList<>();
        List<T> entityList = new ArrayList<>();
        CacheOptResult<List<T>, List<X>> rlt = new CacheOptResult<>();
        rlt.setNoCacheId(noCacheIdList);
        rlt.setCacheData(entityList);

        //转换key
        List<String> keyList = keyList(idList);
        //获取值
        List<String> redisList = redisTemplate.opsForValue().multiGet(keyList);
        for (int i = 0; i < idList.size(); i++) {
            String temp = redisList.get(i);
            if (temp == null) {
                noCacheIdList.add(idList.get(i));
                continue;
            }
            //防止击穿的默认值测试
            if (temp.equals("")) {
                continue;
            }
            //转换为对象
            entityList.add(JsonUtils.toObject(temp, cls));
        }
        return rlt;
    }


    /**
     * @param idList
     * @return object[0]  缓存中的对象  object[1] 缓存中不存在的对象Id
     */
    protected <X, T> CacheOptResult<List<T>, List<X>> baseGet(List<X> idList, TypeReference<T> typeRef) {
        //构造返回值
        List<X> noCacheIdList = new ArrayList<>();
        List<T> entityList = new ArrayList<>();
        CacheOptResult<List<T>, List<X>> rlt = new CacheOptResult<>();
        rlt.setNoCacheId(noCacheIdList);
        rlt.setCacheData(entityList);

        //转换key
        List<String> keyList = keyList(idList);
        //获取值
        List<String> redisList = redisTemplate.opsForValue().multiGet(keyList);
        for (int i = 0; i < idList.size(); i++) {
            String temp = redisList.get(i);
            if (temp == null) {
                noCacheIdList.add(idList.get(i));
                continue;
            }
            //防止击穿的默认值测试
            if (temp.equals("")) {
                continue;
            }
            //转换为对象
            T entity = JsonUtils.complexToObject(temp, typeRef);
            entityList.add(entity);
        }
        return rlt;
    }

    public void baseDelete(Object id) {
        String key = this.key(id);
        redisTemplate.delete(key);
    }


    public void baseDelete(List idList) {
        if (idList.size() == 0) {
            return;
        }
        List<String> keyList = keyList(idList);
        redisTemplate.delete(keyList);
    }


    public List<String> keyList(List idList) {
        List<String> keyList = new ArrayList<>();
        for (Object id : idList) {
            keyList.add(key(id));
        }
        return keyList;
    }

    public String key(Object id) {
        if (id instanceof Long || id instanceof Integer) {
            return prefix + ":" + this.getClass().getSimpleName() + ":" + String.valueOf(id);
        } else if (id instanceof String) {
            return prefix + ":" + this.getClass().getSimpleName() + ":" + id;
        } else {
            throw new DataException("缓存主键仅仅支持String,Long,Integer");
        }

    }

}
