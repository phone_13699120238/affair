package com.zy123.affair.cache.common;

import lombok.Data;

@Data
public class CacheOptResult<T,X> {
    private T cacheData; //缓存中的对象
    private X noCacheId; //缓存中

}
