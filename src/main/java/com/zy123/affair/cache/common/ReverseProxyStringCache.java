package com.zy123.affair.cache.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 创建者： 李树涛   时间： 2021/4/27 10:34
 * 描述：
 * TODO
 */
public abstract class ReverseProxyStringCache<K> extends CommonStringCache {
    public String get(K id) {
        //查询缓存
        CacheOptResult<String, K> cacheRlt = this.baseGet(id);
        //缓存中对象
        String cacheData = cacheRlt.getCacheData();
        //缓存中不存在的id
        K noCacheId = cacheRlt.getNoCacheId();
        if (noCacheId == null) {
            return cacheData;
        }

        String data = getData(id);

        if(data == null){
            this.baseSetAndTime(id,"");
        }else{
            this.baseSetAndTime(id,data);
        }
        return data;
    }

    public List<String> getList(List<K> idList) {
        //查询缓存
        CacheOptResult<List<String>, List<K>> cacheRlt = this.baseGet(idList);
        //缓存中对象
        List<String> cacheData = cacheRlt.getCacheData();
        //缓存中不存在的id
        List<K> noCacheId = cacheRlt.getNoCacheId();
        if (noCacheId.size() == 0) {
            return cacheData;
        }

        Map<K, String> map = getDataToMap(noCacheId);
        this.baseSetAndTime(map);

        //检查不存在的key,防止击穿攻击
        Map<K,String> noDbMap=new HashMap<>(noCacheId.size());
        for(K tempId:noCacheId){
            String entity = map.get(tempId);
            if(entity == null){
                noDbMap.put(tempId,"");
            }
        }
        this.baseSetAndTime(noDbMap);

        cacheData.addAll(map.values());
        return  cacheData;
    }


    public void reset(List<K> idList){
        //查询缓存
        //转换key
        List<String> keyList = keyList(idList);
        //获取值
        List<K> cacheIdList=new ArrayList<>(idList.size());
        List<String> redisList = redisTemplate.opsForValue().multiGet(keyList);
        for (int i = 0; i < idList.size(); i++) {
            String temp = redisList.get(i);
            if (temp == null) {
                continue;
            }
            //防止击穿的默认值测试
            if (temp.equals("")) {
                continue;
            }
            cacheIdList.add(idList.get(i));
        }

        //存在的缓存数量为0,不更新
        if(cacheIdList.size() == 0){
            return;
        }
        Map<K, String> map = getDataToMap(cacheIdList);
        this.baseSetAndTime(map);
    }

    protected abstract String getData(K keyId);

    protected abstract Map<K, String> getDataToMap(List<K> keyId);

}
