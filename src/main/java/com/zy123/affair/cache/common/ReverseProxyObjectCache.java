package com.zy123.affair.cache.common;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 创建者： 李树涛   时间： 2021/4/27 10:34
 * 描述：
 * TODO
 */
public abstract class ReverseProxyObjectCache<K,V> extends CommonStringCache {
    private Class<V> valueCls;

    public ReverseProxyObjectCache(){
        valueCls = (Class<V>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[1];
    }
    public V get(K id) {
        //查询缓存
        CacheOptResult<V, K> cacheRlt = this.baseGet(id, valueCls);
        //缓存中对象
        V cacheData = cacheRlt.getCacheData();
        //缓存中不存在的id
        K noCacheId = cacheRlt.getNoCacheId();
        if (noCacheId == null) {
            return cacheData;
        }

        V data = getData(id);

        if(data == null){
            this.baseSetAndTime(id,"");
        }else{
            this.baseSetAndTime(id,data);
        }
        return data;
    }

    public List<V> getList(List<K> idList) {
        //查询缓存
        CacheOptResult<List<V>, List<K>> cacheRlt = this.baseGet(idList, valueCls);
        //缓存中对象
        List<V> cacheData = cacheRlt.getCacheData();
        //缓存中不存在的id
        List<K> noCacheId = cacheRlt.getNoCacheId();
        if (noCacheId.size() == 0) {
            return cacheData;
        }

        Map<K, V> map = getDataToMap(noCacheId);
        this.baseSetAndTime(map);

        //检查不存在的key,防止击穿攻击
        Map<K,String> noDbMap=new HashMap<>(noCacheId.size());
        for(K tempId:noCacheId){
            V entity = map.get(tempId);
            if(entity == null){
                noDbMap.put(tempId,"");
            }
        }
        this.baseSetAndTime(noDbMap);

        cacheData.addAll(map.values());

        return  cacheData;
    }

    public void reset(List<K> idList){
        //查询缓存
        //转换key
        List<String> keyList = keyList(idList);
        //获取值
        List<K> cacheIdList=new ArrayList<>(idList.size());
        List<String> redisList = redisTemplate.opsForValue().multiGet(keyList);
        for (int i = 0; i < idList.size(); i++) {
            String temp = redisList.get(i);
            if (temp == null) {
                continue;
            }
            cacheIdList.add(idList.get(i));
        }

        //存在的缓存数量为0,不更新
        if(cacheIdList.size() == 0){
            return;
        }
        Map<K, V> map = getDataToMap(cacheIdList);

        //更新数据为空，缓存设置为空
        Map<K,String> noDbMap=new HashMap<>(cacheIdList.size());
        for(K tempId:cacheIdList){
            V entity = map.get(tempId);
            if(entity == null){
                noDbMap.put(tempId,"");
            }
        }
        this.baseSetAndTime(noDbMap);


        this.baseSetAndTime(map);
    }


    public void reset(K id){
        //查询缓存
        //转换key
        String key = key(id);
        //获取值
        String strValue = redisTemplate.opsForValue().get(key);
        if (strValue == null) {
            return;
        }
        V data = getData(id);
        if(data == null){
            this.baseSetAndTime(id,"");
        }else{
            this.baseSetAndTime(id,data);
        }
    }

    protected abstract V getData(K keyId);

    protected abstract Map<K, V> getDataToMap(List<K> keyIdList);

}
